﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprinkleParticleManager : MonoBehaviour
{
    //public GameObject Stand;
    public ParticleSystem sprinkleparticle;
    public Color colorOne;
    public Color colorTwo;
    public Color colorApplied;
    public float hue, H,S,V, H3,H4;

    public Color[] sourceColors;

    public bool updateNeeded;
    public bool near;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
        if (updateNeeded)
        {
            AssignColors();
            updateNeeded = false;
        }
        if(colorApplied != IcePicker.instance.buttonImages[0].color)
        {
            updateNeeded = true;
        }

    }

    public void AssignColors()
    {
        sourceColors[0] = IcePicker.instance.buttonImages[0].color;
        sourceColors[1] = IcePicker.instance.buttonImages[1].color;

        float  H1, S1, V1, H2, S2, V2, S3, V3, S4, V4;
        Color.RGBToHSV(sourceColors[0], out H1, out S1, out V1);
        Color.RGBToHSV(sourceColors[1], out H2, out S2, out V2);
        if(Mathf.Abs(H2 - H1) > 0.3f)
        {
            near = false;
            H3 = ((H1 + H2) / 2);
            H4 = H2 + (H2 - H3)+ 0.1f;
            if (H4 < 0)
            {
                H4 = 1 + H4;
            }
            else if (H4 > 1)
            {
                H4 = H4 - 1;
            }
        }
        else
        {
            near = true;
            if(H1< H2)
            {
                H3 = H1 - 0.3f;
                H4 = H2 + 0.3f;
            }
            else
            {
                H4 = H1 - 0.3f;
                H3 = H2 + 0.3f;
            }

            if (H3 < 0)
            {
                H3 = 1 + H3;
            }
            else if (H4 > 1)
            {
                H3 = H3 - 1;
            }
            if (H4 < 0)
            {
                H4 = 1 + H4;
            }
            else if (H4 > 1)
            {
                H4 = H4 - 1;
            }
        }
        

        colorOne = Color.HSVToRGB(H3,1,1);

        colorTwo = Color.HSVToRGB(H4, 1, 1);

        var main = sprinkleparticle.main;

        main.startColor = new ParticleSystem.MinMaxGradient(colorOne, colorTwo);

        colorApplied = IcePicker.instance.buttonImages[0].color;
    }
}
