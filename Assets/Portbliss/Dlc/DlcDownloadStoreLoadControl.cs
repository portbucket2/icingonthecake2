﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
using System.IO;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace Portbliss.DLC
{
    public class DlcDownloadStoreLoadControl : MonoBehaviour
    {
        //const bool logEnabled = true;
        Action<bool> savedCallback;

        void DlcJobDone(bool allComplete)
        {
            savedCallback?.Invoke(allComplete);
            savedCallback = null;
            StopAllCoroutines();
        }

        public void DownloadAndStoreIfReq(Action<bool> OnComplete)
        {
            savedCallback = OnComplete;
            StartCoroutine(DownloadDLC());
        }
        
        IEnumerator DownloadDLC()
        {
            yield return null;
            if (DlcManager.WillUseSharedBundle)
            {
                yield return StartCoroutine(DowloadShared());
            }

            DLC_Descriptor[] descriptors = DlcManager.AllDlcDescription.AllDescriptors;
            if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>we will try to download dlc one by one.</color>"); }
            for (int i = 0; i < descriptors.Length; i++)
            {
                bool exist = descriptors[i].DoesItExist();
                if (exist)
                {
                    continue;
                }
                else
                {
                    if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>will try to download level: "+descriptors[i].LevelNum+"</color>"); }
                    DLC_Descriptor desc = descriptors[i];
                    AssetBundle.UnloadAllAssetBundles(false);
                    Resources.UnloadUnusedAssets();
                    yield return StartCoroutine(DownloadParticularDlc(desc));
                    if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>completed dlc operation of level: " + desc.LevelNum + "</color>"); }
                }
            }
            DlcJobDone(true);
        }

        IEnumerator DownloadParticularDlc(DLC_Descriptor descriptor)
        {
            yield return null;
            descriptor.DoesExistAsHD_Flag.value = false;

            byte[] content = null;
            using (UnityWebRequest www = UnityWebRequest.Get(descriptor.WebUrl))
            {
                if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>obtained main level bundle fetch request for level: "+descriptor.LevelNum+"</color>"); }
                
                var op = www.SendWebRequest();
                var prog = DlcManager.AllDlcDescription.ProgressDescription.GetProgressDescByIdentifier(AsyncTaskType.DownloadDlc, descriptor.LevelNum);
                DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = prog;
                prog.asyncOp = op;
                yield return op;
                if (www.isNetworkError || www.isHttpError)
                {
                    if (DlcManager.DlcLogEnabled) { Debug.Log("<color='red'>pkg download error: " + www.error + " for level: "+descriptor.LevelNum+"</color>"); }
                    DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = null;
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = true;
                    descriptor.DoesExistAsHD_Flag.value = false;
                    DlcJobDone(false);
                }
                else
                {
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = false;
                    if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>we have downloaded the dlc content bytes successfully! for level: " + descriptor.LevelNum + "</color>"); }
                    content = www.downloadHandler.data;
                }
            }

            Crc32 c = new Crc32();
            yield return null;
            uint crcNumber = c.ComputeChecksum(content);
            yield return null;
            if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>fully downloaded the level bundle data. lets write it into the disk. for level: "+descriptor.LevelNum+"</color>"); }
            
            if (DlcManager.IsCrcCheckImportant)
            {
                if (DlcManager.CrcData == null)
                {
                    if (DlcManager.DlcLogEnabled) { Debug.Log("<color='red'>crc data is null for level: " + descriptor.LevelNum + "</color>"); }
                    descriptor.DoesExistAsHD_Flag.value = false;
                    DlcJobDone(false);
                }
                else
                {
                    uint thisFileCrc = DlcManager.CrcData.GetOriginalCrc(descriptor.LevelNum);
                    if (DlcManager.DlcLogEnabled) { Debug.LogWarning("downloaded content's crc: " + crcNumber + " and the original file's crc: " + thisFileCrc + " for level: " + descriptor.LevelNum); }
                    if (thisFileCrc == crcNumber)
                    {
                        if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>crc matched so lets write stuffs for level: " + descriptor.LevelNum + " </color>"); }
                        yield return StartCoroutine(DoWriteStuffs(descriptor, content));
                    }
                    else
                    {
                        if (DlcManager.DlcLogEnabled) { Debug.Log("<color='red'>crc did not match for level: " + descriptor.LevelNum + "</color>"); }
                        descriptor.DoesExistAsHD_Flag.value = false;
                        DlcJobDone(false);
                    }
                }
            }
            else
            {
                if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>we do not need to check for crc, lets just write stuff for level: " + descriptor.LevelNum + "</color>"); }
                yield return StartCoroutine(DoWriteStuffs(descriptor, content));
            }
        }

        IEnumerator DoWriteStuffs(DLC_Descriptor descriptor, byte[] content)
        {
            if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>will write the stuffs </color>"); }
            FileInfo file = new FileInfo(descriptor.DiskUrl);
            file.Directory.Create(); // If the directory already exists, this method does nothing.
            yield return new WaitForSeconds(0.7f);
            try
            {
                File.WriteAllBytes(descriptor.DiskUrl, content);
                descriptor.DoesExistAsHD_Flag.value = true;
            }
            catch (Exception ex)
            {
                if (DlcManager.DlcLogEnabled) { Debug.Log("<color='red'>dlc write into disk error: " + ex.Message + " for level: " + descriptor.LevelNum + "</color>"); }
                descriptor.DoesExistAsHD_Flag.value = false;
                DlcJobDone(false);
            }

            if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>just written the dlc stuff </color>"); }
            if (DlcManager.DlcLogEnabled)
            {
                Debug.Log("<color='green'>level bundle data has been written into the disk.</color>");
            }

            yield return new WaitForSeconds(0.7f);

#if UNITY_IOS
        Device.SetNoBackupFlag(descriptor.DiskUrl);
#endif
        }

        IEnumerator DowloadShared()
        {
            yield return null;
            DlcManager.AllDlcDescription.HasSharedDlcDownloaded.value = false;

            string sharedUrl = DlcManager.BaseSrvUrlWithEndSlash + "shared.gpkg";
            byte[] content = null;
            using (UnityWebRequest www = UnityWebRequest.Get(sharedUrl))
            {
                if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>obtained shared bundle fetch request.</color>"); }

                var op = www.SendWebRequest();
                var prog = DlcManager.AllDlcDescription.ProgressDescription.GetProgressDescByIdentifier(AsyncTaskType.SharedDlc);
                DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = prog;
                prog.asyncOp = op;
                yield return op;
                if (www.isNetworkError || www.isHttpError)
                {
                    if (DlcManager.DlcLogEnabled) { Debug.Log("<color='red'>shared dlc download error!</color>"); }
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = true;
                    DlcManager.AllDlcDescription.HasSharedDlcDownloaded.value = false;
                    DlcJobDone(false);
                }
                else
                {
                    if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>we have downloaded the shared dlc content bytes successfully!</color>"); }
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = false;
                    content = www.downloadHandler.data;
                }
            }

            Crc32 c = new Crc32();
            yield return null;
            uint crcNumber = c.ComputeChecksum(content);
            yield return null;
            if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>fully downloaded the shared bundle data. lets write it into the disk.</color>"); }

            if (DlcManager.IsCrcCheckImportant)
            {
                if (DlcManager.CrcData == null)
                {
                    if (DlcManager.DlcLogEnabled) { Debug.Log("<color='red'>crc data is null for shared dlc.</color>"); }
                    DlcManager.AllDlcDescription.HasSharedDlcDownloaded.value = false;
                    DlcJobDone(false);
                }
                else
                {
                    uint thisFileCrc = DlcManager.CrcData.GetOriginalCrcForSharedDlc();
                    if (DlcManager.DlcLogEnabled) { Debug.LogWarning("downloaded shared content's crc: " + crcNumber + " and the original file's crc: " + thisFileCrc); }
                    if (thisFileCrc == crcNumber)
                    {
                        if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>crc matched for shared dlc, so lets write stuffs.</color>"); }
                        yield return StartCoroutine(WriteShared(content));
                    }
                    else
                    {
                        if (DlcManager.DlcLogEnabled) { Debug.Log("<color='red'>crc did not match for shared dlc</color>"); }
                        DlcManager.AllDlcDescription.HasSharedDlcDownloaded.value = false;
                        DlcJobDone(false);
                    }
                }
            }
            else
            {
                if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>we do not need to check for crc, lets just write stuff for shared dlc</color>"); }
                yield return StartCoroutine(WriteShared(content));
            }
        }

        IEnumerator WriteShared(byte[] content)
        {
            if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>will write the stuffs for shared dlc. </color>"); }
            string shDiskUrl = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName + "/shared.gpkg");

            FileInfo file = new FileInfo(shDiskUrl);
            file.Directory.Create(); // If the directory already exists, this method does nothing.
            yield return new WaitForSeconds(0.7f);
            try
            {
                File.WriteAllBytes(shDiskUrl, content);
                DlcManager.AllDlcDescription.HasSharedDlcDownloaded.value = true;
            }
            catch (Exception ex)
            {
                if (DlcManager.DlcLogEnabled) { Debug.Log("<color='red'>shared dlc write into disk error: " + ex.Message + " for shared dlc.</color>"); }
                DlcManager.AllDlcDescription.HasSharedDlcDownloaded.value = false;
                DlcJobDone(false);
            }

            if (DlcManager.DlcLogEnabled) { Debug.Log("<color='green'>just written the shared dlc stuff </color>"); }
            if (DlcManager.DlcLogEnabled)
            {
                Debug.Log("<color='green'>shared level bundle data has been written into the disk.</color>");
            }

            yield return new WaitForSeconds(0.7f);

#if UNITY_IOS
        Device.SetNoBackupFlag(shDiskUrl);
#endif
        }
    }
}