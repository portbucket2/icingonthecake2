﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Networking;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace Portbliss.DLC
{
    public class CrcControl : MonoBehaviour
    {
        int attemptNum = 0;
        Action<DescriptionOfOriginalCrcData> savedCallback;

        void CrcDone(DescriptionOfOriginalCrcData crcData)
        {
            if (crcData == null)
            {
                DlcManager.AllDlcDescription.WillWeDownloadCrcData.value = true;
            }
            
            savedCallback?.Invoke(crcData);
            savedCallback = null;
            StopAllCoroutines();
        }

        public void DownloadAndStoreIfReq_AndGetCRC_Data(Action<DescriptionOfOriginalCrcData> OnComplete)
        {
            attemptNum = 0;
            savedCallback = OnComplete;
            string dataFolderPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            string diskUrl = Path.Combine(dataFolderPath, "crc.txt");
            StartCoroutine(DoStuffs(diskUrl));
        }

        IEnumerator DoStuffs(string diskUrl)
        {
            if (DlcManager.AllDlcDescription.IsThereAnyCrcOnDiskAndIsItValid())
            {
                if (DlcManager.CrcLogEnabled) { Debug.Log("<color='green'>crc json local file found, lets fetch it.</color>"); }
                yield return StartCoroutine(LoadCrcListFromFile(diskUrl, (crcData) =>
                {
                    CrcDone(crcData);
                }));

                yield return new WaitForSeconds(0.7f);
            }
            else
            {
                //start manifest download coroutine
                StartCoroutine(DownloadCrcList());
            }
        }

        IEnumerator LoadCrcListFromFile(string diskUrl, Action<DescriptionOfOriginalCrcData> loadFromDiskCB)
        {
            var uri = new Uri(diskUrl).AbsoluteUri;
            yield return StartCoroutine(DownloadCrcFromDisk(uri, diskUrl, loadFromDiskCB));
        }

        IEnumerator DownloadCrcFromDisk(string uri, string fileSysPath, Action<DescriptionOfOriginalCrcData> loadFromDiskCB)
        {
            DescriptionOfOriginalCrcData result = null;
            if (DlcManager.CrcLogEnabled) { Debug.Log("<color='green'>let us download crc json file from file system</color>"); }
            using (UnityWebRequest www = UnityWebRequest.Get(uri))
            {
                var op = www.SendWebRequest();
                var prog = DlcManager.AllDlcDescription.ProgressDescription.GetProgressDescByIdentifier(AsyncTaskType.CrcLoadFromDisk);
                DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = prog;
                prog.asyncOp = op;
                yield return op;
                if (DlcManager.CrcLogEnabled) { Debug.Log("<color='green'>crc Json file from from file system downloaded into memory.</color>"); }
                if (www.isNetworkError || www.isHttpError)
                {
                    DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = null;
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = true;
                    if (DlcManager.CrcLogEnabled) { Debug.Log("<color='red'>crc list download from disk error: " + www.error + "</color>"); }
                    loadFromDiskCB?.Invoke(null);
                }
                else
                {
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = false;
                    string crcJson = www.downloadHandler.text;
                    try
                    {
                        result = JsonUtility.FromJson<DescriptionOfOriginalCrcData>(crcJson);
                        if (DlcManager.CrcLogEnabled) { Debug.Log("<color='green'>crc Json file to crc data conversion successfull</color>"); }
                        loadFromDiskCB?.Invoke(result);
                    }
                    catch (Exception ex)
                    {
                        if (DlcManager.CrcLogEnabled) { Debug.Log("<color='red'>Could not load crc data from json file</color>"); }
                        loadFromDiskCB?.Invoke(null);
                    }
                }
            }
        }

        IEnumerator DownloadCrcList()
        {
            string dataPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            string diskUrl = Path.Combine(dataPath, "crc.txt");
            string webUrl = Path.Combine(DlcManager.BaseSrvUrlWithEndSlash, "crc.txt");

            if (DlcManager.CrcLogEnabled) { Debug.Log("<color='green'>Now let us download crc list from web and write it onto disk.</color>"); }
            
            using (UnityWebRequest www = UnityWebRequest.Get(webUrl))
            {
                var op = www.SendWebRequest();
                var prog = DlcManager.AllDlcDescription.ProgressDescription.GetProgressDescByIdentifier(AsyncTaskType.CrcDownload);
                DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = prog;
                prog.asyncOp = op;
                yield return op;
                if (www.isNetworkError || www.isHttpError)
                {
                    if (DlcManager.CrcLogEnabled) { Debug.Log("<color='red'>crc list download error: " + www.error + "</color>"); }
                    DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = null;
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = true;
                    if (attemptNum > DlcManager.CrcDownloadAttemptMax)
                    {
                        if (DlcManager.CrcLogEnabled) { Debug.Log("<color='red'>crc list file download abort. poor net may be.</color>"); }
                        CrcDone(null);
                    }
                    else
                    {
                        attemptNum++;
                        if (DlcManager.CrcLogEnabled) { Debug.Log("<color='yellow'>lets try again for crc download.</color>"); }
                        yield return StartCoroutine(DownloadCrcList());
                    }
                }
                else
                {
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = false;
                    FileInfo file = new FileInfo(diskUrl);
                    file.Directory.Create(); // If the directory already exists, this method does nothing.
                    yield return new WaitForSeconds(0.7f);
                    try
                    {
                        File.WriteAllBytes(diskUrl, www.downloadHandler.data);
                        DlcManager.AllDlcDescription.WillWeDownloadCrcData.value = false;
                    }
                    catch (Exception ex)
                    {
                        if (DlcManager.CrcLogEnabled) { Debug.Log("<color='red'>crc write into file error: " + ex.Message + "</color>"); }
                        CrcDone(null);
                    }

                    if (DlcManager.CrcLogEnabled) { Debug.Log("<color='green'>crc list file written.</color>"); }
                    yield return StartCoroutine(LoadCrcListFromFile(diskUrl, (crcData) =>
                    {
                        CrcDone(crcData);
                    }));

#if UNITY_IOS
                    Device.SetNoBackupFlag(diskUrl);
#endif
                    if (DlcManager.CrcLogEnabled) { Debug.Log("<color='green'>crc list file location has been patched for itune issue.</color>"); }
                }
            }

            yield return null;
        }
    }
}