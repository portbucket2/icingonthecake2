﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IPHONE || UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;
using System.Collections.Generic;

namespace Portbliss.Ad
{
    public class PlistManagement : MonoBehaviour
    {

#if UNITY_IOS

        [PostProcessBuild]
        static void OnPostprocessBuild(BuildTarget buildTarget, string path)
        {
            // Read plist
            var plistPath = Path.Combine(path, "Info.plist");
            var plist = new PlistDocument();
            plist.ReadFromFile(plistPath);

            // Update value
            PlistElementDict rootDict = plist.root;
            rootDict.SetString("NSCalendarsUsageDescription", "Store calendar events from ads");
            rootDict.SetString("NSPhotoLibraryUsageDescription", "For gif sharing");
            rootDict.SetString("NSLocationWhenInUseUsageDescription", "Used to deliver better advertising experience");
            PlistElementDict NSAppTransportSecurity = rootDict.CreateDict("NSAppTransportSecurity");
            NSAppTransportSecurity.SetBoolean("NSAllowsArbitraryLoads", true);

            //arm issue code start
            foreach (var v in rootDict.values)
            {
                if (v.Key == "UIRequiredDeviceCapabilities")
                {
                    rootDict.values.Remove("UIRequiredDeviceCapabilities");
                    break;
                }
            }
            var elemArray = rootDict.CreateArray("UIRequiredDeviceCapabilities");
            elemArray.AddString("armv7");
            //arm issue code end
            

            // remove exit on suspend if it exists.
            string exitsOnSuspendKey = "UIApplicationExitsOnSuspend";
            if (rootDict.values.ContainsKey(exitsOnSuspendKey))
            {
                rootDict.values.Remove(exitsOnSuspendKey);
            }

            // Write plist
            File.WriteAllText(plistPath, plist.WriteToString());


            ////bitcode stuffs
            //PBXProject proj = new PBXProject();
            //proj.ReadFromFile(PBXProject.GetPBXProjectPath(path));
            ////Get the build target
            //string target = proj.TargetGuidByName("Unity-iPhone");

            //proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            //string content = proj.WriteToString();
            //byte[] contentBytes = System.Text.Encoding.Default.GetBytes(content);
            //FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Write);
            //fs.Write(contentBytes, 0, contentBytes.Length);
            //fs.Close();

            ////File.WriteAllText(path, proj.WriteToString());
        }
#endif
    }
}