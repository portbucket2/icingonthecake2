﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
using com.adjust.sdk;

namespace Portbliss.Ad
{
    public delegate void OnCompleteAdFuc(bool success);
    public enum AdIntensityType { Normal, Medium, Aggresive, Invalid }
    public partial class AdController : MonoBehaviour
    {
        const bool logEnabled = false;
        public static AdController instance;
        const string sdkKey = "Lzi5VR_J50y55PM5ctwAwALT5d9g1CKMhT1TF0naOa4fSUn98Vd6rXsvAp4I3A-5LaPvNk4RSvKe5fesxKhRzh";
        static bool isSDKready = false;
        public static bool IsSDK_Ready { get { return isSDKready; } }
        public static bool AllowSkipFromMax = true;
#if UNITY_ANDROID
        const string interstitialAdUnitId = "f0b43043cc5b6e47";
#elif UNITY_IOS
        const string interstitialAdUnitId = "0813a19339ead372";
#else
        const string interstitialAdUnitId = "0813a19339ead372";
#endif

#if UNITY_ANDROID
        const string rewardedAdUnitId = "433dbb3d12aee8e1";
#elif UNITY_IOS
        const string rewardedAdUnitId = "5d13a5738571dfbd";
#else
        const string rewardedAdUnitId = "433dbb3d12aee8e1";
#endif

#if UNITY_ANDROID
        const string bannerAdUnitId = "75a42afdba4727c9"; // Retrieve the id from your account
#elif UNITY_IOS
        const string bannerAdUnitId = "d7f01a51b7a4c79b"; // Retrieve the id from your account
#else
        const string bannerAdUnitId = "d7f01a51b7a4c79b"; // Retrieve the id from your account
#endif
        [SerializeField] Color bannerColor = Color.white;
        [SerializeField] bool useInterstitial = true, useRewardedVideoAd = false, useBanner = true;
        [SerializeField] MaxSdk.BannerPosition bannerPosition = MaxSdkBase.BannerPosition.BottomCenter;
        [SerializeField] bool testModeShowMediationDebugger = false;
        [SerializeField] bool ForceLocationEU_Test = false;
        [SerializeField] GDPRController GDPR_Script;
        OnCompleteAdFuc intersitialDel, rewardedDel;
        static bool hasShownVideoAd = false, hasDismissedVideoAd = false;
        static bool hasShownInterstitial = false, hasDismissedIntersitial = false;
        static bool isShowingBanner = false;
        public static bool IsBanerAdShowing { get { return isShowingBanner; } }
        public static bool gdpr_done
        {
            get; private set;
        }

        private static HardData<bool> _isUserEU;
        public static bool isUserEU
        {
            get
            {
                if (_isUserEU == null) _isUserEU = new HardData<bool>("IS_USER_EU", false);
                return _isUserEU.value;
            }
            set
            {
                if (_isUserEU == null) _isUserEU = new HardData<bool>("IS_USER_EU", false);
                _isUserEU.value = value;
            }
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartAdSystem ();
                DontDestroyOnLoad(this);
            }
            else
            {
                if (gameObject != null)
                {
                    DestroyImmediate(gameObject);
                }
                else
                {
                    DestroyImmediate(this);
                }
            }
        }

        public static void CompleteConsentTask()
        {
            
            #if !UNITY_EDITOR
                MaxSdk.SetHasUserConsent(GDPRController.instance.HasAdPermission);	
            #endif
            if (GDPRController.instance.HasAnalyticsPermission == false)
            {
                if (GDPRController.instance.AdjustForgetUserHD.value == false)
                {
                    #if !UNITY_EDITOR
                        Adjust.gdprForgetMe();	
                    #endif
                    Debug.Log("USER FORGOT!!!!!!!!!!!!!!!!!!!!!");
                }
                GDPRController.instance.ForgetUserPermanently_Adjust();
            }
            Debug.Log("<color='red'>permission window done!</color>");
            GameConfig.hasDoneConsentRelatedTasksHD.value = GDPRController.instance.HasCompleteConsent;
            gdpr_done = true;
        }

        private void OnDisable ()
        {
            GDPR_Script.OnGDPR_UI_Completion -= CompleteConsentTask;
        }
        
        void StartAdSystem()
        {
#if UNITY_IOS || UNITY_ANDROID
            gdpr_done = false;
            GDPR_Script.OnGDPR_UI_Completion += CompleteConsentTask;
            isShowingBanner = false;
            isSDKready = false;
            TimedAd.InitSys();
            MaxSdk.SetSdkKey(sdkKey);
            MaxSdk.InitializeSdk();
            
            //
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                

                InitializeAndPreloadSelectedAdTypes();
                // AppLovin SDK is initialized, start loading ads
                isSDKready = true;
                LogUtil.Green("ad sdk has been initialized!", true);
                //HardData<bool> isUserEU = new HardData<bool>("IS_USER_EU", false);

                if (GameConfig.hasDoneConsentRelatedTasksHD.value == false )
                {
                    if ( sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies || ForceLocationEU_Test )
                    {
                        if (GDPR_Script == null)
                        {
                            GameConfig.hasDoneConsentRelatedTasksHD.value = true;
                            gdpr_done = true;
                        }
                        else
                        {
                            // Show user consent dialog
                            GDPR_Script.StartGDPRFromWelcome();
                        }
                        isUserEU = true;
                    }
                    else if ( sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.DoesNotApply )
                    {
                        //consentIsRequired = false;
                        // No need to show consent dialog, proceed with initialization
                        GameConfig.hasDoneConsentRelatedTasksHD.value = true;
                        gdpr_done = true;
                    }
                    else
                    {
                        // Consent dialog state is unknown. Proceed with initialization, but check if the consent
                        // dialog should be shown on the next application initialization
                        GameConfig.hasDoneConsentRelatedTasksHD.value = false;
                        gdpr_done = true;
                    }
                }
                else
                {
                    gdpr_done = true;
                }
                
                foreach (var s in ABManager.allSettings)
                {
                    string varName = s.Value.GetID();
                    string varValue = "";

                    varValue = MaxSdk.VariableService.GetString(varName);

                    Debug.Log("<color='magenta'>variable name: " + varName + " and variable value: " + varValue + "</color>");
                    s.Value.Assign_IfUnassigned(varValue);
                }
                ABManager.SetFetchComplete();

                //Debug.Log("<color='green'>the variable type from max sdk is:" + variableValue + "</color>");
                //var SkipVariableValue = MaxSdk.VariableService.GetString("Allow_Skip");

                //if(SkipVariableValue== "1")
                //{
                //    AllowSkipFromMax = true;
                //    //InLevelCanvasManager.instance.AllowSkip = true;
                //    FacebookManager.LogSkipMode(SkipVariableValue);
                //    Debug.Log("_______ALLOWING SKIP!!!!!");
                //}

                //if(SkipVariableValue=="0")
                //{
                //    AllowSkipFromMax = false;
                //    //InLevelCanvasManager.instance.AllowSkip = false;
                //    FacebookManager.LogSkipMode(SkipVariableValue);
                //    Debug.Log("_______NOT ALLOWING SKIP!!!!!");
                //}

                if (testModeShowMediationDebugger)
                {
                    MaxSdk.ShowMediationDebugger();
                }
            };
#endif
        }

        void InitializeAndPreloadSelectedAdTypes()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (useInterstitial)
            {
                InitializeInterstitialAds();
            }

            if (useRewardedVideoAd)
            {
                InitializeRewardedAds();
            }

            if (useBanner)
            {
                InitializeBannerAds();
            }
#endif
        }

        public static void ShowInterstitialAd(OnCompleteAdFuc OnComplete)
        {
            if (instance.intersitialDel != null)
            {
                Debug.Log("<color='red'>previous interstitial callback has not been fired. can not call the method again.</color>");
                return;
            }

#if UNITY_IOS || UNITY_ANDROID
            hasShownInterstitial = hasDismissedIntersitial = false;
            Debug.Log("<color='magenta'>so interstitial ad id: " + interstitialAdUnitId + " and is sdk ready? "
            + isSDKready + " is that interstitial ad ready? " + MaxSdk.IsInterstitialReady(interstitialAdUnitId) + "</color>");
            if (MaxSdk.IsInterstitialReady(interstitialAdUnitId) && isSDKready)
            {
                MaxSdk.ShowInterstitial(interstitialAdUnitId);
                instance.intersitialDel = OnComplete;
            }
            else
            {
                instance.intersitialDel = null;
                OnComplete?.Invoke(false);
            }
#else
            OnComplete?.Invoke(true);
#endif

        }

        public static void ShowRewardedVideoAd(OnCompleteAdFuc OnComplete)
        {
            if (instance.rewardedDel != null)
            {
                Debug.Log("<color='red'>previous rewarded ad callback has not been fired. can not call the method again.</color>");
                return;
            }

#if UNITY_IOS || UNITY_ANDROID
            hasShownVideoAd = hasDismissedVideoAd = false;
            Debug.Log("<color='magenta'>so rewarded ad id: " + rewardedAdUnitId + " and is sdk ready? "
            + isSDKready + " is that rewarded ad ready? " + MaxSdk.IsRewardedAdReady(rewardedAdUnitId) + "</color>");
            if (MaxSdk.IsRewardedAdReady(rewardedAdUnitId) && isSDKready)
            {
                MaxSdk.ShowRewardedAd(rewardedAdUnitId);
                instance.rewardedDel = OnComplete;
            }
            else
            {
                instance.rewardedDel = null;
                OnComplete?.Invoke(false);
            }
#else
            OnComplete?.Invoke(true);
#endif
        }

        public static void ShowBanner()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (isSDKready && isShowingBanner == false)
            {
                isShowingBanner = true;
                MaxSdk.ShowBanner(bannerAdUnitId);
            }
#endif
        }

        public static void HideBanner()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (isSDKready && isShowingBanner == true)
            {
                isShowingBanner = false;
                MaxSdk.HideBanner(bannerAdUnitId);
            }
#endif
        }
    }
}