﻿using System.Collections;
using System.Collections.Generic;
using FRIA;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.Ad
{
    public class GDPR_SettingsButtonControl : MonoBehaviour
    {
        [SerializeField] Button gdprButtonInSettings;
        void Start()
        {
            gdprButtonInSettings.gameObject.SetActive(false);
            //HardData<bool> isUserEU = new HardData<bool>("IS_USER_EU", false);
            //if (isUserEU.value)
            //{
            //    gdprButtonInSettings.gameObject.SetActive(true);
            //    gdprButtonInSettings.onClick.AddListener(() =>
            //    {
            //        GDPRController.instance.StartGDPRFromSettings();
            //    });
            //}

            StartCoroutine(CheckGDPR());
        }

        IEnumerator CheckGDPR()
        {
            yield return null;
            while (AdController.gdpr_done == false)
            {
                yield return null;
            }
            if (AdController.instance != null)
            {
                if (GDPRController.instance.ShowAnnoyance.value == 0 || GDPRController.instance.ShowAnnoyance.value == 1)
                {
                    gdprButtonInSettings.gameObject.SetActive(true);
                    gdprButtonInSettings.onClick.AddListener(() =>
                    {
                        //GDPRController.instance.StartGDPRFromSettings();
                        OnGDPR_FlowRequest();
                    });
                }
            }
        }

        private void OnGDPR_FlowRequest()
        {
            if (GDPRController.isGDPRFlowOnRequestActive) return;
            GDPRController.instance.StartGDPRFromUserRequest();
            //OnSettingsCloseButton();
        }
    }
}