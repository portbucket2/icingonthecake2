﻿using System.Collections;
using System.Collections.Generic;
using FRIA;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.IAP
{
    public class IAP_No_AdButtonBinder : MonoBehaviour
    {
        [SerializeField] Button noAdBtn;
        [SerializeField] Button[] allBtnOfNoAd;
        [SerializeField] GameObject whiteBG;
        // Start is called before the first frame update
        void Start ()
        {
            noAdBtn.gameObject.SetActive(true);
            if (GameConfig.hasIAP_NoAdPurchasedHD.value == true)
            {
                noAdBtn.gameObject.SetActive(false);
                whiteBG.SetActive(false);
            }
            else
            {
                StartCoroutine(CheckForNoAdIAP());
            }
            
            noAdBtn.onClick.RemoveAllListeners ();
            noAdBtn.onClick.AddListener ( () =>
            {
                if (IAP_Controller.instance != null)
                {
                    IAP_Controller.instance.Buy_NoAd((success) =>
                    {
                        if (success)
                        {
                            noAdBtn.gameObject.SetActive(false);
                            whiteBG.SetActive(false);
                            foreach (var v in allBtnOfNoAd)
                            {
                                v.gameObject.SetActive(false);
                            }
                        }
                    });
                }
            } ); 
        }

        IEnumerator CheckForNoAdIAP()
        {
            while (IAP_Controller.hasAdPurchased == false)
            {
                yield return null;
            }
            noAdBtn.gameObject.SetActive(false);
            whiteBG.SetActive(false);
            foreach (var v in allBtnOfNoAd)
            {
                v.gameObject.SetActive(false);
            }
        }
    }

}
