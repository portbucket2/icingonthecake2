﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.IAP
{
    [System.Serializable]
    public class SubscriptionDesc
    {
        [SerializeField] string subscriptionProdID, appleSubscriptionName, googleSubscriptionName;
        public string SubscriptionProdID { get { return subscriptionProdID; } }
        public string AppleSubscriptionName { get { return appleSubscriptionName; } }
        public string GoogleSubscriptionName { get { return googleSubscriptionName; } }
    }
    public delegate void Func1(string prodID);
    public delegate void Func2(string prodID, bool isPurchaseEvent, bool purchaseSuccess);

    public interface IIAP_Buy
    {
        void BuyProduct(string prodID);
    }
}