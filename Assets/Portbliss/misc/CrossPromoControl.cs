﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Ad;
using UnityEngine.UI;
using FRIA;
using Portbliss.IAP;
//using Portbliss.IAP;

public class CrossPromoControl : MonoBehaviour
{
    // Start is called before the first frame update

    // Top-left X position expressed as a percentage of the screen width.
    const float xPercent = 10f;

    // Top-left Y position expressed as a percentage of the screen height
    const float yPercent = 40f;

    // Width expressed as a percentage of the minimum value between screen width and height
    const float wPercent = 45.0f;

    // Height expressed as a percentage of the minimum value between screen width and height
    const float hPercent = 45.0f;

    // Clock-wise rotation expressed in degrees
    const float rDegrees = -15f;

    
    private void Awake()
    {
        AppLovinCrossPromo.Init();

    }

    private void Start()
    {
        //if ( !( LevelLoader.Last_li == 0 && LevelLoader.Last_ai == 0 ) )
        //{

        //}
        //Invoke("ShowPromo", 0.1f);
        StartCoroutine (CheckForSDKReadyAndShowPromo());
        StartCoroutine(CheckForNoAdIAP());
    }

    IEnumerator CheckForNoAdIAP()
    {
        while(IAP_Controller.hasAdPurchased == false)
        {
            yield return null;
        }
        AdController.HideBanner();
    }

    IEnumerator CheckForSDKReadyAndShowPromo()
    {
        while(true)
        {
            if(AdController.IsSDK_Ready 
                && UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 1 
                && AdController.gdpr_done)
            {
                break; 
            }
            yield return null;
        }

        yield return new WaitForSeconds(0.1f);

        //Invoke("ShowPromo", 0.1f);
        //Centralizer.Add_DelayedMonoAct(this, ShowPromo,0.1f);
        if (!GameConfig.hasIAP_NoAdPurchasedHD.value)
        {
            Debug.Log("<color='magenta'>now we will call promo method</color>");
            AppLovinCrossPromo.Instance().ShowMRec(xPercent, yPercent, wPercent, hPercent, rDegrees);
            AdController.ShowBanner();
        }
    }
}
