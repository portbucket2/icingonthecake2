﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.Social
{
    public class GifShareControl : MonoBehaviour
    {
        static GifShareControl instance;
        static bool isRecording = false;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                if (instance.gameObject != gameObject)
                {
                    DestroyImmediate(this);
                }
            }
        }

        void Start()
        {
            // Receiver's callback for when a share has been opened
            Megacool.Instance.ReceivedShareOpened += (MegacoolReceivedShareOpenedEvent megacoolEvent) => {
                Debug.Log("Got event: " + megacoolEvent);
                if (megacoolEvent.IsFirstSession)
                {
                    // This device has received a share and installed the
                    // app for the first time
                    Debug.Log("Installed from a referral from " + megacoolEvent.SenderUserId);
                }
            };
            Megacool.Instance.SentShareOpened += (MegacoolSentShareOpenedEvent megacoolEvent) => {
                Debug.Log("Got event: " + megacoolEvent);
                if (megacoolEvent.IsFirstSession)
                {
                    // A share sent from this device has been opened, and
                    // the receiver installed the app for the first time
                    Debug.Log(megacoolEvent.ReceiverUserId + " installed the app from our referral");
                }
            };
            // Initialize the Megacool SDK. The callbacks must be
            // registered before this.
            Megacool.Instance.Start();
            Megacool.Instance.CaptureMethod = MegacoolCaptureMethod.BLIT;
            isRecording = false;
        }

        public static void ShareGif()
        {
            //Megacool.Instance.Share();
            Debug.Log("<color='magenta'>now we will share our generated gif or share fallback gif</color>");
#if UNITY_EDITOR
            return;
#endif
            Debug.Log("<color='magenta'>now we will share our generated gif or share fallback gif RUNTIME</color>");

            Megacool.Instance.Share(new MegacoolShareConfig()
            {
                // Overrides the default "Come play with me!" set above for
                // this specific share
                ModalTitle = "Ice the cake!",
                Message = "Ice the cake!",
                Strategy = MegacoolSharingStrategy.MEDIA,
                FallbackImage = "a.gif"
            });

        }

        public static void BeginRecord()
        {

            if (isRecording) { return; }
            isRecording = true;

            Debug.Log("<color='magenta'>now we will start recording for gif!</color>");
#if UNITY_EDITOR
            return;
#endif
            Debug.Log("<color='magenta'>now we will start recording for gif! RUNTIME</color>");
            Megacool.Instance.StartRecording();
        }

        public static void EndRecord()
        {

            if (!isRecording) { return; }
            isRecording = false;

            Debug.Log("<color='magenta'>now we will end recording for gif!</color>");
#if UNITY_EDITOR
            return;
#endif
            Debug.Log("<color='magenta'>now we will end recording for gif! RUNTIME</color>");
            Megacool.Instance.StopRecording();
        }
    }
}