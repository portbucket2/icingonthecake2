﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TriggerAreaDetector : MonoBehaviour , IPointerExitHandler, IPointerDownHandler , IPointerUpHandler, IPointerEnterHandler
{
    //public static bool 
    public static bool pressedOn;
   // public static TriggerAreaDetector instance;
    void Awake()
    {
        //instance = this;
        pressedOn = false;
    }
    void OnDisable()
    {
        pressedOn = false;
    }
    public void OnPointerExit(PointerEventData eventData)
    {

        pressedOn = false;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        pressedOn = true;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        pressedOn = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(Input.GetMouseButton(0))
            pressedOn = true;
    }
}