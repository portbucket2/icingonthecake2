﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class RotationSpeedManager : MonoBehaviour
{

    public static RotationSpeedManager instance;
    private Vector3 currentRotationSpeed = new Vector3(0, 135, 0);
    public Vector3 baseRotationSpeed
    {
        get
        {
            if (PhaseManager.instance)
            {
                currentRotationSpeed = Vector3.Lerp(currentRotationSpeed, PhaseManager.instance.baseRotationSpeed, 5 * Time.deltaTime);
            }
            return currentRotationSpeed;
        }
    }

    static HardData<int> savedSpeedLevel;
    RotationSpeedLevel speedLevel
    {
        get { return (RotationSpeedLevel)savedSpeedLevel.value; }
        set { savedSpeedLevel.value = (int)value; }
    }
    public Transform rotationTarget;
    public Transform camTrans;

    [Header("Speed Panel")]
    public Color enabledColor = Color.black;
    public Color disabledColor = Color.gray;
    public Button[] plusbuttons;
    public Image plusMainImage;
    public Button[] minusbuttons;
    public Image minusMainImage;
    public Text speedText;

    float dampingRatio = 1;
    public Vector3 SpeedValueForLevel
    {
        get
        {
            switch (speedLevel)
            {
                //case RotationSpeedLevel.neg_150:
                //    return baseRotationSpeed * -1.5f;
                //case RotationSpeedLevel.neg_100:
                //    return baseRotationSpeed * -1.0f;
                //case RotationSpeedLevel.neg_050:
                //    return baseRotationSpeed * -0.5f;
                case RotationSpeedLevel.pos_050:
                    return baseRotationSpeed * 0.5f;
                case RotationSpeedLevel.pos_100:
                    return baseRotationSpeed * 1.0f;
                case RotationSpeedLevel.pos_150:
                    return baseRotationSpeed * 1.5f;
                case RotationSpeedLevel.pos_200:
                    return baseRotationSpeed * 2.0f;
                case RotationSpeedLevel.pos_075:
                    return baseRotationSpeed * 0.75f;
                default:
                    return baseRotationSpeed;
            }
        }

    }
    public Vector3 ChosenSpeed
    {
        get
        {
            return SpeedValueForLevel * (1 - dampingRatio);
        }
    }
    public static float ChosenRPM
    {
        get { return instance.ChosenSpeed.y / 360; }
    }
    public string speedString
    {
        get
        {
            switch (speedLevel)
            {
                //case RotationSpeedLevel.neg_150:
                //    return baseRotationSpeed * -1.5f;
                //case RotationSpeedLevel.neg_100:
                //    return baseRotationSpeed * -1.0f;
                //case RotationSpeedLevel.neg_050:
                //    return baseRotationSpeed * -0.5f;
                case RotationSpeedLevel.pos_050:
                    return "Speed x0.5";
                case RotationSpeedLevel.pos_100:
                    return "Speed x1";
                case RotationSpeedLevel.pos_150:
                    return "Speed x1.5";
                case RotationSpeedLevel.pos_200:
                    return "Speed x2";
                case RotationSpeedLevel.pos_075:
                    return "Speed x0.75";
                default:
                    return "Speed x1";
            }
        }

    }

    int max;
    private void Awake()
    {
        instance = this;
        dampingRatio = 0;
        if (savedSpeedLevel == null) savedSpeedLevel = new HardData<int>("SPEED_LEVEL", (int)RotationSpeedLevel.pos_100);
        max = System.Enum.GetValues(typeof(RotationSpeedLevel)).Length;

        foreach ( Button b in plusbuttons )
        {
            b.onClick.AddListener ( () => OnChoiceChanged ( true ) );
        }
        foreach ( Button b in minusbuttons )
        {
            b.onClick.AddListener ( () => OnChoiceChanged ( false ) );
        }
        speedText.text = speedString;

        int currentIndex = (int)speedLevel;
        foreach ( Button b in plusbuttons )
        {
            b.interactable = currentIndex != max - 1;
        }
        foreach ( Button b in minusbuttons )
        {
            b.interactable = currentIndex != 0;
        }

        plusMainImage.color = ( plusbuttons[0].interactable ? enabledColor : disabledColor);
        minusMainImage.color = ( minusbuttons[0].interactable ? enabledColor : disabledColor);
        SetNewTargets(0, 100);
    }

    void OnChoiceChanged(bool positive)
    {
        int currentIndex = (int)speedLevel;

        currentIndex += (positive ? 1 : -1);
        speedLevel = (RotationSpeedLevel)currentIndex;

        speedText.text = speedString;

        foreach ( Button b in plusbuttons )
        {
            b.interactable = currentIndex != max - 1;
        }
        foreach ( Button b in minusbuttons )
        {
            b.interactable = currentIndex != 0;
        }

        plusMainImage.color = ( plusbuttons[0].interactable ? enabledColor : disabledColor);
        minusMainImage.color = ( minusbuttons[0].interactable ? enabledColor : disabledColor);

        AnalyticsAssistant.SpeedChanged();
    }


    void Update()
    {
        if (!MainGameManager.settings.editorPaintMode)
        {
            rotationTarget.transform.Rotate(ChosenSpeed * Time.deltaTime);
            camTrans.localPosition = Vector3.Lerp(camTrans.localPosition, posTarget, rateMultiplier * posLerpRate * Time.deltaTime);
            camTrans.localRotation = Quaternion.Lerp(camTrans.localRotation, rotTarget, rateMultiplier * locLerpRate * Time.deltaTime);
        }
        else
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rotationTarget.transform.Rotate(ChosenSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                rotationTarget.transform.Rotate(-ChosenSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                camTrans.localPosition = Vector3.Lerp(camTrans.localPosition, instance.paintcamPos2.localPosition, 3 * posLerpRate * Time.deltaTime);
                camTrans.localRotation = Quaternion.Lerp(camTrans.localRotation, instance.paintcamPos2.localRotation, 3 * locLerpRate * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                camTrans.localPosition = Vector3.Lerp(camTrans.localPosition, instance.camPos1.localPosition, 3 * posLerpRate * Time.deltaTime);
                camTrans.localRotation = Quaternion.Lerp(camTrans.localRotation, instance.camPos1.localRotation, 3 * locLerpRate * Time.deltaTime);
            }
        }


    }
    //public static void Reset()
    //{
    //    instance.camTrans.localPosition = instance.camPos1.localPosition;
    //    instance.camTrans.localRotation = instance.camPos1.localRotation;
    //}

    Vector3 posTarget;
    Quaternion rotTarget;
    public Transform camPos1;
    public Transform camPos2;
    public Transform paintcamPos2;
    public float posLerpRate = 1;
    public float locLerpRate = 1;
    public float rateMultiplier = 1;
    public static void SetNewTargets(float fraction, float rateMult)
    {
        //Debug.Log("F: " + fraction);
        instance.posTarget = Vector3.Lerp(instance.camPos1.localPosition, instance.camPos2.localPosition, fraction);
        instance.rotTarget = Quaternion.Lerp(instance.camPos1.localRotation, instance.camPos2.localRotation, fraction);
        instance.rateMultiplier = rateMult;

    }
    public static void SetNewTargets(Transform recordedTarget, float rateMult)
    {
        //Debug.Log("T: "+ recordedTarget);
        instance.posTarget = recordedTarget.localPosition;
        instance.rotTarget = recordedTarget.localRotation;
        instance.rateMultiplier = rateMult;
    }
    Coroutine rotationBreaker;
    public void BreakRotation(float changingTime, bool reverseBreak)
    {
        if (rotationBreaker != null) StopCoroutine(rotationBreaker);

        rotationBreaker = StartCoroutine(RotationBreak(changingTime, reverseBreak));
    }

    IEnumerator RotationBreak(float changingTime, bool reverseBreak)
    {
        float startTime = Time.time;
        while ((dampingRatio < 1 && !reverseBreak) || (reverseBreak && dampingRatio > 0))
        {
            float timePassed = Time.time - startTime;

            if (!reverseBreak)
                dampingRatio = Mathf.Lerp(0, 1, Mathf.Clamp01(timePassed / changingTime));
            else

                dampingRatio = Mathf.Lerp(1, 0, Mathf.Clamp01(timePassed / changingTime));
            yield return null;
        }
    }
}

public enum RotationSpeedLevel
{
    //neg_150,
    //neg_100,
    //neg_050,
    pos_050,
    pos_075,
    pos_100,
    pos_150,
    pos_200,
}