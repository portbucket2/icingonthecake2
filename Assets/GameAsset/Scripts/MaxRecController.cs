﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxRecController : MonoBehaviour
{
#if UNITY_ANDROID
    public const string adID = "e5a7d018609e8e7c";
#elif UNITY_IOS
    public const string adID = "f2d1aba7f46c2628";
#endif


    //void Start()
    //{
    //    MaxSdk.CreateMRec(adID, MaxSdkBase.AdViewPosition.TopCenter);
    //    MaxSdk.ShowMRec(adID);
    //}



    static bool initialzed = false;
    static bool gameThinksEnabled = false;
    static bool sdkThinksEnabled = false;

    public bool log;

    private void Awake()
    {
        if (initialzed)
        {
            DestroyImmediate(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            initialzed = true;
        }
    }

    IEnumerator Start()
    {

        MaxSdk.CreateMRec(adID, MaxSdkBase.AdViewPosition.BottomCenter);
        MaxSdk.HideMRec(adID);
        sdkThinksEnabled = false;
        gameThinksEnabled = false;

        //AppLovinCrossPromo.Instance().ShowMRec(X_POS, Y_POS, WIDTH, HEIGHT, ROTATION);
        WaitForSeconds wfs1 = new WaitForSeconds(2);
        while (true)
        {
            if (sdkThinksEnabled != gameThinksEnabled)
            {
                sdkThinksEnabled = gameThinksEnabled;
                if (sdkThinksEnabled)
                {
                    if (log) Debug.Log("<color='magenta'>Max MREC enabled</color>");
                    MaxSdk.ShowMRec(adID);
                    yield return wfs1;//since desyncing is dangerous
                }
                else
                {
                    if (log) Debug.Log("<color='magenta'>Max MREC disabled</color>");
                    MaxSdk.HideMRec(adID);
                    yield return wfs1;//since desyncing is dangerous
                }
            }
            else
            {
                if (!sdkThinksEnabled)
                {
                    if (log) Debug.Log("<color='magenta'>Max MREC disable enforced</color>");
                    MaxSdk.HideMRec(adID);
                    yield return wfs1;
                }
                else yield return null;
            }
        }
    }



    public static void Active(bool enable)
    {
        if (GameConfig.hasIAP_NoAdPurchasedHD.value && enable)
        {
            return;
        }
        if (gameThinksEnabled != enable)
        {
            gameThinksEnabled = enable;

        }
    }

}
