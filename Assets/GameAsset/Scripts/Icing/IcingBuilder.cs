﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class IcingBuilder : ScriptableObject
{
    //public static int peelCounter =0;
    //public float peelOffset;
    public bool needsVisualUpdate;
    //public const float PEEL_WIDTH = 0.075f;
    public const float EXTRUDE_CONST = 0.002f;//.002f;
    public const float JUMP_LIMIT = 0.500f;

    public const int colliderInterval = 5;

    public float travelLimit = 0.03f;
    public Transform root;
    //public Rigidbody rgbd;
    public GameObject front;
    public GameObject back;
    public float currentWindValue;
    public float currentWindEndTime;
    public float ejectYpos;
    public float ejectTime;
    public float standardHalfWidth;

    public List<Transform> colliders=new List<Transform>();

    public int colChannelIndex;

    Mesh fiberFrontMesh;
    //Mesh fiberBackMesh;

    public List<IcingFibers> fibers = new List<IcingFibers>();


    #region easy access functions
    public IcingFibers lastFiber_0
    {
        get
        {
            if (fibers.Count < 1) return null;
            else return fibers[fibers.Count - 1];
        }
    }
    public IcingFibers lastFiber_1
    {
        get
        {
            if (fibers.Count < 2) return null;
            else return fibers[fibers.Count - 2];
        }
    }
    public IcingFibers lastFiber_2
    {
        get
        {
            if (fibers.Count < 3) return null;
            else return fibers[fibers.Count - 3];
        }
    }
    public float GetTotalFiberLength()
    {
        float length = 0;
        for (int i = 0; i < fibers.Count; i++)
        {
            length += fibers[i].lastRecordedLength;
        }
        return length;
    }
    public Vector3 GetTravelVec()
    {
        return lastFiber_0.basePoint - lastFiber_1.basePoint;
    }
    public Vector3 GetTravelVec_1()
    {
        return lastFiber_1.basePoint - lastFiber_2.basePoint;
    }
    public Vector3 GetPotentialTravelVec(Vector3 pointInMeshSpace)
    {
        return pointInMeshSpace - lastFiber_1.basePoint;
    }
    public float GetLastInterFiberDistance()
    {
        return (lastFiber_0.basePoint - lastFiber_1.basePoint).magnitude;
    }
    public float GetPotentialStrechDistance(Vector3 worldPoint)
    {
        //root.InverseTransformPoint(lastFiber_0.point);
        return (worldPoint - root.TransformPoint(lastFiber_1.basePoint)).magnitude;
    }
    public bool WillExceedTravelLimit(Vector3 worldPoint)
    {
        //Debug.LogFormat("Travel: {0}, Count: {1}", GetCurrentTravelLength(), fibers.Count);
        return GetPotentialStrechDistance(worldPoint) > travelLimit;
    }
    public bool WillExceedJumpLimit(Vector3 worldPoint)
    {
        //Debug.LogFormat("Travel: {0}, Count: {1}", GetCurrentTravelLength(), fibers.Count);
        return GetPotentialStrechDistance(worldPoint) > JUMP_LIMIT;
    }
    #endregion


    public IcingBuilder(PhaseManager phaseMan, float travelLimit,float standardHalfWidth, int ARC_VxCount )
    {
        this.standardHalfWidth = standardHalfWidth;
        //peelOffset = 0.001f*(peelCounter+1);
        //peelCounter++;
        //this.ARC_N = ARC_VxCount;
        this.travelLimit = travelLimit;



        root = new GameObject("peel").transform;
        front = new GameObject("front");
        back = new GameObject("back");
        front.transform.SetParent(root);
        back.transform.SetParent(root);
        root.position = (PhaseManager.CurrentPhaseController as PhaseController_Icing).targetChoppedMesh.transform.position;
        root.rotation = (PhaseManager.CurrentPhaseController as PhaseController_Icing).targetChoppedMesh.transform.rotation;



        MeshFilter frontFilter = front.AddComponent<MeshFilter>();
        MeshRenderer frontRenderer = front.AddComponent<MeshRenderer>();
        frontRenderer.receiveShadows = false;
        frontRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        fiberFrontMesh = new Mesh();
        fiberFrontMesh.name = "fberFrontMesh";
        frontFilter.mesh = fiberFrontMesh;
        
        frontRenderer.material = IcePicker.currentMat;
        colChannelIndex = (int) IcePicker.currentChannel;

        startFibers = new IcingFibers[EDGE_N];
        endFibers = new IcingFibers[EDGE_N];


        //front.gameObject.SetLayer("PrimaryOnly", false);
    }

    IcingFibers[] startFibers;
    IcingFibers[] endFibers;
    public void AddFiber(RaycastHit hit, float halfPeelWidth, Vector2 uvU, Vector2 uvD)
    {
        lastLength = 0;

        IcingFibers pf = new IcingFibers(hit, root, halfPeelWidth);

        if (fibers.Count >= 2)
        {
            Vector3 travelDirection = fibers[fibers.Count - 1].basePoint - fibers[fibers.Count - 2].basePoint;
            pf.upOffset = Vector3.Cross(travelDirection.normalized, pf.hitNormal).normalized * pf.halfPeelWidth;
        }

        fibers.Add(pf);
        UpdateLastFibers(hit,uvU,uvD);

    }

    float lastLength = 0;
    public void UpdateLastFibers(RaycastHit hit, Vector2 uvU, Vector2 uvD)
    {
        lastFiber_0.UpdateUV(uvU, uvD);
        if (fibers.Count < 2) return;


        lastFiber_0.basePoint = root.InverseTransformPoint(hit.point + hit.normal * EXTRUDE_CONST);
        Vector3 travelDirection = lastFiber_0.basePoint - lastFiber_1.basePoint;
        lastFiber_0.lastRecordedLength = travelDirection.magnitude;

        if (lastFiber_0.lastRecordedLength == 0) return;

        lastFiber_0.SetUpOffset( Vector3.Cross(travelDirection.normalized, lastFiber_0.hitNormal).normalized * lastFiber_0.halfPeelWidth);
        if (lastFiber_2 == null)
            lastFiber_1.SetUpOffset(Vector3.Cross(travelDirection.normalized, lastFiber_1.hitNormal).normalized * lastFiber_1.halfPeelWidth);
        else
        {
            Vector3 travelDirection2 = lastFiber_1.basePoint - lastFiber_2.basePoint;
            lastFiber_1.SetUpOffset(Vector3.Cross((travelDirection.normalized + travelDirection2.normalized) / 2, lastFiber_1.hitNormal).normalized * lastFiber_1.halfPeelWidth);

        }
        //Debug.Log(lastFiber_1.upOffset.magnitude);


        float currentLength = GetLastInterFiberDistance();
        lastLength = GetLastInterFiberDistance();

        lastFiber_0.UpdateAllVerticesWithExtendedElements();
        lastFiber_1.UpdateAllVerticesWithExtendedElements();
    }

    public float EjectPeel(bool disablePhysics, float ejectForce)
    {
        visualFiber.Clear();
        GenerateFarEnds();
        visualFiber.AddRange(startFibers);
        visualFiber.AddRange(fibers);
        visualFiber.AddRange(endFibers);
        fibers = visualFiber;
        visualFiber = new List<IcingFibers>();

        ejectYpos = root.position.y;
        ejectTime = Time.time;
        //if (!disablePhysics)
        //{
        //    rgbd = root.gameObject.AddComponent<Rigidbody>();
        //    rgbd.AddForce( root.TransformDirection( lastFiber_0.hitNormal)* ejectForce, ForceMode.Impulse);
        //}
        return GetTotalFiberLength();
    }
    public void UnloadPoolColliders()
    {
        for (int i = colliders.Count-1; i >=0; i--)
        {
            
            FRIA.Pool.Destroy(colliders[i].gameObject,true);
        }
    }
    public void TrashItems()
    {
        UnloadPoolColliders();
        if (fiberFrontMesh) Destroy(fiberFrontMesh);
        if(root)DestroyImmediate(root.gameObject);
    }

    #region draw
    //int ARC_N = 24;
    public void UpdateFiberMesh(bool excludeEnds)
    {
        UpdateFrontMesh(excludeEnds);
        //UpdateBackMesh();
        needsVisualUpdate = false;
    }
    int EDGE_N =6;

    public List<IcingFibers> visualFiber = new List<IcingFibers>(1000);

    void GenerateFarEnds()
    {
        Vector3 startOffset = (fibers[0].basePoint - fibers[1].basePoint).normalized * standardHalfWidth*1f;
        Vector3 endOffset = (lastFiber_0.basePoint - lastFiber_1.basePoint).normalized * standardHalfWidth*1f;
        //Debug.Log("gotit");
        //Debug.Log(fibers.Count);
        for (int i = 0; i < EDGE_N; i++)
        {
            float f = i / ((float)EDGE_N);
            float offsetFrac = 1 - f;
            float scaleFrac;
            if (f < 0.5f)
            {
                scaleFrac = Mathf.Pow(2*f, 1.35f)/2;
            }
            else
            {
                scaleFrac = (Mathf.Pow(2*f-1, 0.35f)+1)/2;
            }
            //float SP = 0.4f;
            //float CP = 1 - SP;
            //if (f < SP)
            //{
            //    scaleFrac = Mathf.Pow(f / SP, 1.2f) * SP;
            //}
            //else
            //{
            //    scaleFrac = (Mathf.Pow((f - SP) / CP, 0.2f) * CP) + SP;
            //}
            //fraction = Mathf.Pow( fraction,1);
            //Debug.Log(scaleFrac);
            if (scaleFrac == 0) scaleFrac = 0.01f;
            startFibers[i] =  new IcingFibers(fibers[0], scaleFactor:  scaleFrac, startOffset*offsetFrac);
            endFibers[EDGE_N - i-1] = new IcingFibers(lastFiber_0, scaleFactor:  scaleFrac, (endOffset )*offsetFrac);
        }
    }
    //void GenerateFarEnds()
    //{
    //    Vector3 startOffset = (fibers[0].basePoint - fibers[1].basePoint).normalized * standardHalfWidth;
    //    Vector3 endOffset = (lastFiber_0.basePoint - lastFiber_1.basePoint).normalized * standardHalfWidth;
    //    //Debug.Log(fibers.Count);
    //    for (int i = 1; i <= EDGE_N; i++)
    //    {
    //        float f = i / ((float)EDGE_N);
    //        float scaleFrac = Mathf.Pow(f, 1f);
    //        float offsetFrac = 1 - f;
    //        //fraction = Mathf.Pow( fraction,1);

    //        startFibers[i - 1] = new IcingFibers(fibers[0], scaleFactor: f, startOffset * offsetFrac);
    //        endFibers[i - 1] = new IcingFibers(lastFiber_0, scaleFactor: f, (endOffset + lastFiber_0.hitNormal * standardHalfWidth) * offsetFrac);
    //    }
    //}
    private void UpdateFrontMesh(bool excludeEnds)
    {
        List<IcingFibers> meshFibers;
        if (excludeEnds)
        {
            meshFibers = fibers;
        }
        else
        {
            GenerateFarEnds();
            visualFiber.Clear();
            visualFiber.AddRange(startFibers);
            visualFiber.AddRange(fibers);
            visualFiber.AddRange(endFibers);
            meshFibers = visualFiber;
        }

        //if(!excludeEnds)GenerateFarEnds();
        //visualFiber.Clear();
        //if (!excludeEnds) visualFiber.AddRange(startFibers);
        //visualFiber.AddRange(fibers);
        //if (!excludeEnds) visualFiber.AddRange(endFibers);

        int ARC_N = IcingFibers.ARC_N;
        int used_VXIndex = meshFibers.Count * ARC_N;
        int used_TRIndex = (meshFibers.Count - 1)*(ARC_N) * 6;
        Vector3[] vertices = new Vector3[used_VXIndex];// + ARC_N * 4];
        Vector3[] normals = new Vector3[used_VXIndex];// + ARC_N * 4];
        Vector2[] uv = new Vector2[used_VXIndex];// + ARC_N * 4];
        int[] triangles = new int[used_TRIndex];// + (ARC_N *3*3+6)*2];


        for (int f = 0; f < meshFibers.Count; f++)
        {
            for (int i = 0; i < ARC_N; i++)
            {
                vertices[ARC_N * f + i] = meshFibers[f].vertices[i];
                normals[ARC_N * f + i] = meshFibers[f].normals[i];
                uv[ARC_N * f + i] = meshFibers[f].uvs[i];
            }

            if (f == meshFibers.Count - 1)
            {
                continue;
            }
            else
            {
                for (int i = 0; i < ARC_N; i++)
                {
                    if (i == ARC_N - 1)
                    {
                        triangles[f * (ARC_N) * 6 + 6 * i + 0] = (f + 0) * ARC_N + i + 0;
                        triangles[f * (ARC_N) * 6 + 6 * i + 1] = (f + 1) * ARC_N + 0;
                        triangles[f * (ARC_N) * 6 + 6 * i + 2] = (f + 0) * ARC_N + 0;

                        triangles[f * (ARC_N) * 6 + 6 * i + 3] = (f + 0) * ARC_N + i + 0;
                        triangles[f * (ARC_N) * 6 + 6 * i + 4] = (f + 1) * ARC_N + i + 0;
                        triangles[f * (ARC_N) * 6 + 6 * i + 5] = (f + 1) * ARC_N + 0;
                    }
                    else
                    {
                        triangles[f * (ARC_N ) * 6 + 6 * i + 0] = (f + 0) * ARC_N + i + 0;
                        triangles[f * (ARC_N ) * 6 + 6 * i + 1] = (f + 1) * ARC_N + i + 1;
                        triangles[f * (ARC_N) * 6 + 6 * i + 2] = (f + 0) * ARC_N + i + 1;

                        triangles[f * (ARC_N) * 6 + 6 * i + 3] = (f + 0) * ARC_N + i + 0;
                        triangles[f * (ARC_N) * 6 + 6 * i + 4] = (f + 1) * ARC_N + i + 0;
                        triangles[f * (ARC_N) * 6 + 6 * i + 5] = (f + 1) * ARC_N + i + 1;
                    }
                }
            }
        }

        fiberFrontMesh.vertices = vertices;
        fiberFrontMesh.normals = normals;
        fiberFrontMesh.triangles = triangles;
        fiberFrontMesh.uv = uv;
    }
    #endregion


}