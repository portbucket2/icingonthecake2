﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcingMaker
{
    public IcingBuilder icingBuilder;
    public IcingSettings settings;
    public Transform ejectRoot;
    public IcingMaker(IcingSettings settings, Transform ejectRoot)
    {
        this.ejectRoot = ejectRoot;
        this.settings = settings;
    }
    public void IcingUpdate(RaycastHit hit, Vector2 uvUp, Vector2 uvDown, float brushRad)
    {
        float standardHalfPeelWidth = brushRad * 1.07f;

        //float customHalfPeelWidth;
        //if (icingBuilder == null)
        //{
        //    customHalfPeelWidth = standardHalfPeelWidth * .01f;
        //}
        //else
        //{
        //    //float currentLength = icingBuilder.GetTotalFiberLength();
        //    //if (currentLength < settings.transitionLength)
        //    //{
        //    //    customHalfPeelWidth = standardHalfPeelWidth * Mathf.Clamp01(currentLength / settings.transitionLength);
        //    //}
        //    //else if (currentLength > settings.lengthCutOffLimit - settings.transitionLength)
        //    //{
        //    //    customHalfPeelWidth = standardHalfPeelWidth * Mathf.Clamp01((settings.lengthCutOffLimit - currentLength) / settings.transitionLength);
        //    //}
        //    //else
        //    {
        //    }

        //}
        float customHalfPeelWidth = standardHalfPeelWidth;
        customHalfPeelWidth = Handy.Deviate(customHalfPeelWidth, 0.05f);

        if (icingBuilder == null)
        {
            icingBuilder = new IcingBuilder(PhaseManager.instance, settings.peelDivisionLength,standardHalfPeelWidth, settings.arcVertices);
            icingBuilder.AddFiber(hit, customHalfPeelWidth, uvUp, uvDown);
            icingBuilder.AddFiber(hit, customHalfPeelWidth, uvUp, uvDown);

        }
        else
        {
            //Debug.Log("==========================B");
            bool angleBreak = false;
            if (icingBuilder.fibers.Count > 2)
            {
                if (icingBuilder.GetTravelVec().sqrMagnitude > 0)
                {
                    Vector3 point = icingBuilder.root.InverseTransformPoint(hit.point);
                    float angle1 = Vector3.Angle(icingBuilder.GetTravelVec(), icingBuilder.GetPotentialTravelVec(point));
                    float angle2 = Vector3.Angle(icingBuilder.GetTravelVec_1(), icingBuilder.GetPotentialTravelVec(point));
                    if (angle1 > settings.angleCutOffLimit)
                    {
                        angleBreak = true;
                    }
                    else if (angle2 > settings.angleCutOffLimit)
                    {
                        angleBreak = true;
                    }
                }


            }
            bool fiberCountBreak = icingBuilder.fibers.Count >= settings.fiberCountCutOffLimit;
            bool jumpLimitBreak = icingBuilder.WillExceedJumpLimit(hit.point);
            if (jumpLimitBreak || angleBreak || fiberCountBreak)
            {
                Eject(string.Format("Jump break {0}, angle Break {1}, fiber count Break {2}", jumpLimitBreak, angleBreak, fiberCountBreak));
                icingBuilder = new IcingBuilder(PhaseManager.instance, settings.peelDivisionLength,standardHalfPeelWidth, settings.arcVertices);
                icingBuilder.AddFiber(hit, customHalfPeelWidth, uvUp, uvDown);
                icingBuilder.AddFiber(hit, customHalfPeelWidth, uvUp, uvDown);
            }
            else if (icingBuilder.WillExceedTravelLimit(hit.point))
            {
                icingBuilder.AddFiber(hit, customHalfPeelWidth, uvUp, uvDown);
                icingBuilder.UpdateLastFibers(hit, uvUp, uvDown);
            }
            else
            {

                //Debug.Log("==========================D");
                icingBuilder.UpdateLastFibers(hit, uvUp, uvDown);
            }

            if (icingBuilder != null && icingBuilder.GetTotalFiberLength() > float.MaxValue) // nextCutOffLength)
            {
                Debug.Log(icingBuilder.GetTotalFiberLength() > float.MaxValue);
                Eject("Peel too long");
            }
        }
        if (icingBuilder != null) icingBuilder.UpdateFiberMesh(false);

    }

    public void Eject(string ejectReason)
    {
        if (icingBuilder == null) return;
        float peelLength = icingBuilder.GetTotalFiberLength();
        icingBuilder.EjectPeel(disablePhysics: true, ejectForce: 1);
        icingBuilder.root.SetParent(ejectRoot);
        PhaseManager.CurrentPhaseController.oldIcings.Add(icingBuilder);
        icingBuilder = null;

        if (MainGameManager.settings.showEjectLog) Debug.LogFormat("Ejected: {0}", ejectReason);


    }

    public static void EndCurrentIcing(string ejectReason)
    {
        if (PhaseManager.instance && PhaseManager.CurrentPhaseController.opCon!=null && PhaseManager.CurrentPhaseController.opCon.icingMaker!=null)
        {
            PhaseManager.CurrentPhaseController.opCon.icingMaker.Eject(ejectReason); ;
        }
    }
    public static bool IsIcingNow()
    {
        if (PhaseManager.instance && PhaseManager.CurrentPhaseController.opCon != null && PhaseManager.CurrentPhaseController.opCon.icingMaker != null)
        {
            return PhaseManager.CurrentPhaseController.opCon.icingMaker.icingBuilder != null;
        }
        else
        {
            return false;
        }
    }



}
