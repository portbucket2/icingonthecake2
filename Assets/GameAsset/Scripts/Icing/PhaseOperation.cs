﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhaseOperation
{
    //private const float RAY_RAD_MULT = 4.5f;
    public static PhaseOperation instance
    {
        get
        {
            return PhaseManager.CurrentPhaseController.opCon;
        }
    }
    public OperationSettings settings;

    float standardHalfPeelWidth;

    internal float progress;

    //MeshFilter meshFilter;

    public List<MorfMesh> morfMeshes;

    public Transform trashRoot;
    public float r_max;
    public float r_min;
    public float groupCheckDistance;

    private UnityEngine.Profiling.CustomSampler sampler_FlattenOp1 = UnityEngine.Profiling.CustomSampler.Create("FlattenOp_1");
    private UnityEngine.Profiling.CustomSampler sampler_FlattenOp2 = UnityEngine.Profiling.CustomSampler.Create("FlattenOp_2");
    private UnityEngine.Profiling.CustomSampler sampler_vShiftOpA = UnityEngine.Profiling.CustomSampler.Create("VshiftOp_A");
    private UnityEngine.Profiling.CustomSampler sampler_vShiftOpB = UnityEngine.Profiling.CustomSampler.Create("VshiftOp_B");
    public PhaseOperation(OperationSettings settings, Transform trashRoot)
    {
        this.settings = settings;
        this.trashRoot = trashRoot;
        morfMeshes = PhaseManager.CurrentPhaseController.morfMeshes;
        //meshFilter = PhaseManager.instance.meshFilter;
        standardHalfPeelWidth = settings.brushRadius * 1.07f;
        r_max = settings.brushRadius * settings.brushRadius;
        r_min = settings.primaryInnerRad * settings.primaryInnerRad * r_max;
        groupCheckDistance = Mathf.Pow(Mathf.Sqrt(r_max) + 0.87f * PhaseManager.unitLength, 2);
    }



    float runningLastCentreOffset;
    float lastWidth;

    #region vertex loop data carriers
    public Vector2 uvUp;
    public Vector2 uvDown;
    public float closestSqrDist;
    public float farthestSqrDist;
    #endregion

    #region vertex loop

    /*public void VertexPaintOperation_Tunnel(RaycastHit hit, Ray ray, Vector3 travelVec, Vector3 travelOrigin)
    {
        int progressIndex = PhaseManager.currentPhaseIndex;

        VertexGroup vg;
        int[] vindices;
        int v;
        //float d = 0;
        float tempSqrDist;
        float colTargetVal;
        Vector3 tempVec;
        Color oldColSamp;
        float oldColTotVal;
        //Debug.Log(vertices.Length);       
        Vector3 A;
        Vector3 B;
        float x, y, z;

        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = hit.transform.InverseTransformDirection(ray.direction).normalized;
        for (int i = 0; i < viGroup.Count; i++)
        {
            vg = viGroup[i];
            tempVec = (vg.groupPostion - rayOrigin);
            tempSqrDist = Vector3.Cross(rayDir, tempVec).sqrMagnitude;
            if (tempSqrDist < groupCheckDistance)
            {
                vindices = vg.vertexIndices;
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];
                    tempVec.x = vertices[v].x - rayOrigin.x;
                    tempVec.y = vertices[v].y - rayOrigin.y;
                    tempVec.z = vertices[v].z - rayOrigin.z;
                    A = rayDir;
                    B = tempVec;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    tempSqrDist = x * x + y * y + z * z;
                    if (tempSqrDist < r2)
                    {
                        //total++;
                        oldColSamp = vcolors[v];
                        oldColTotVal = oldColSamp.r + oldColSamp.g + oldColSamp.b + oldColSamp.a;
                        //dirty++;
                        if (tempSqrDist > r1)
                        {
                            colTargetVal = (r2 - tempSqrDist) / (r2 - r1);

                        }
                        else
                        {
                            colTargetVal = 1;
                        }
                        if (ColChannel.A == IcePicker.currentChannel) Debug.LogError("!");
                        switch (IcePicker.currentChannel)
                        {
                            case ColChannel.R:
                                if (colTargetVal > oldColSamp.r) oldColSamp.r = colTargetVal;
                                break;
                            case ColChannel.G:
                                if (colTargetVal > oldColSamp.g) oldColSamp.g = colTargetVal;
                                break;
                            case ColChannel.B:
                                if (colTargetVal > oldColSamp.b) oldColSamp.b = colTargetVal;
                                break;
                            case ColChannel.A:
                                if (colTargetVal > oldColSamp.a) oldColSamp.a = colTargetVal;
                                break;
                        }
                        vcolors[v] = oldColSamp;


                        if (oldColTotVal <= 0.001f && (oldColSamp.r + oldColSamp.g + oldColSamp.b + oldColSamp.a) > 0.001f)
                        {
                            progress += 1;
                        }

                        tempSqrDist = Vector3.Cross(travelVec, vertices[v] - travelOrigin).sqrMagnitude;
                        //distCumulitive += tempSqrDist;
                        if (tempSqrDist > farthestSqrDist)
                        {
                            uvUp = uvs[v];
                            farthestSqrDist = tempSqrDist;
                        }
                        if (tempSqrDist < closestSqrDist)
                        {
                            uvDown = uvs[v];
                            closestSqrDist = tempSqrDist;
                        }
                    }

                }
            }

        }
    }

    public void VertexShiftOperation_Tunnel(RaycastHit hit, Ray ray, float rayRM, bool isRadial)
    {
        
        int progressIndex = PhaseManager.currentPhaseIndex;
        //shiftOperationCounter++;
        //Debug.Log("Here");
        VertexGroup vg;
        int[] vindices;
        int v;
        //float d = 0;


        Vector3 TV;
        float raySqrDist;
         Vector3 A;
        Vector3 B;
        float x, y, z;
        float pushOutVertexDistance = settings.normalPushOutValue;

        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = hit.transform.InverseTransformDirection(ray.direction).normalized;
        for (int i = 0; i < viGroup.Count; i++)
        {
            vg = viGroup[i];
            TV = (vg.groupPostion - rayOrigin);
            raySqrDist = Vector3.Cross(rayDir, TV).sqrMagnitude;
            if (raySqrDist < groupCheckDistance * rayRM)
            {
                vindices = vg.vertexIndices;
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];
                    TV.x = vertices[v].x - rayOrigin.x;
                    TV.y = vertices[v].y - rayOrigin.y;
                    TV.z = vertices[v].z - rayOrigin.z;
                    A = rayDir;
                    B = TV;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    raySqrDist = x * x + y * y + z * z;
                    if (raySqrDist < r2 * rayRM)
                    {

                        if (!vxMovementCompletion[v])
                        {
                            if (Mathf.Abs(Vector3.Dot(worldUpLocally, normals[v])) < .05f || !isRadial)
                            {

                                vxMovementCompletion[v] = true;

                                vertices[v] = vertices[v] + (isRadial?normals[v]:Vector3.up) * pushOutVertexDistance;
                            }
                        }
                    }

                }
            }

        }

    }
    */

    public void FlattenOp_IP(RaycastHit hit, Ray ray, Vector3 hitNormal_MeshCon, float rayRM, float maxHalfHeight, float FlattenRateMult)
    {
        Vector3 vx;
        Vector3 TV;
        float raySqrDist, rayAlignedDistance;

        Vector3 A;
        Vector3 B;
        float x, y, z;

        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = -hitNormal_MeshCon;// hit.transform.InverseTransformDirection(ray.direction).normalized;
        List<IcingBuilder> oldIcings = PhaseManager.CurrentPhaseController.oldIcings;
        float baseShiftRate = settings.flatteningRate * Time.deltaTime * RotationSpeedManager.ChosenRPM * FlattenRateMult;
        float normalShiftRate = settings.flatteningRate * Time.deltaTime * RotationSpeedManager.ChosenRPM * 25;
        bool isInAlignedRange;
        float radialSqrRatio;
        float deltaTime = Time.deltaTime;
        //float radialRatio;
        float shiftRate;
        foreach (IcingBuilder icing in oldIcings)
        {
            if (icing == icingBuilder) continue;
            foreach (IcingFibers fiber in icing.fibers)
            {
                for (int i = 0; i < fiber.vertices.Length; i++)
                {
                    vx = fiber.vertices[i];
                    TV.x = vx.x - rayOrigin.x;
                    TV.y = vx.y - rayOrigin.y;
                    TV.z = vx.z - rayOrigin.z;
                    A = rayDir;
                    B = TV;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    raySqrDist = x * x + y * y + z * z;
                    rayAlignedDistance = B.x * A.x + B.y * A.y + B.z * A.z;
                    isInAlignedRange = false;
                    if (rayAlignedDistance <= 0)
                    {
                        if ((-rayAlignedDistance) < maxHalfHeight * 2) isInAlignedRange = true;
                    }
                    else
                    {
                        if (rayAlignedDistance < maxHalfHeight) isInAlignedRange = true;
                    }

                    radialSqrRatio = raySqrDist / (r_max * rayRM);
                    if (radialSqrRatio < 1 && isInAlignedRange)//(fiber.updatedNormalHeight[i] > -2*shiftRate)   && 
                    {
                        //radialRatio = Mathf.Sqrt(radialSqrRatio);
                        shiftRate = (1 - radialSqrRatio) * baseShiftRate * fiber.edgeScaleFactor;


                        if (fiber.updatedNormalHeight[i] > 0 && fiber.edgeScaleFactor > 0 && fiber.vTimeBudgetSpent[i]<= IcingFibers.ITERATE_TIME_BUDGET)
                        {
                            fiber.normals[i] = Vector3.Lerp(fiber.normals[i], hitNormal_MeshCon, normalShiftRate * fiber.edgeScaleFactor).normalized;// (fiber.normals[i] + hitNormal*1.5f *Time.deltaTime).normalized;
                            //fiber.vertices[i] -= shiftRate * fiber.edgeScaleFactor * fiber.normals[i];// *fiber.centreOffsetMag[i];
                            fiber.vertices[i].x -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].x + fiber.hitNormal.x);
                            fiber.vertices[i].y -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].y + fiber.hitNormal.y);
                            fiber.vertices[i].z -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].z + fiber.hitNormal.z);

                            fiber.updatedNormalHeight[i] -= shiftRate * fiber.edgeScaleFactor;// * fiber.centreOffsetMag[i];
                            icing.needsVisualUpdate = true;
                            fiber.vertices[i] -= shiftRate * fiber.hitNormal;

                            fiber.vTimeBudgetSpent[i] += deltaTime;
                        }
                        else if (fiber.updatedNormalHeight[i] > -0.5f && fiber.vTimeBudgetSpent[i] <= IcingFibers.ITERATE_TIME_BUDGET)
                        {
                            fiber.vertices[i].x -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].x + fiber.hitNormal.x);
                            fiber.vertices[i].y -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].y + fiber.hitNormal.y);
                            fiber.vertices[i].z -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].z + fiber.hitNormal.z);
                            //fiber.vertices[i] -= shiftRate * fiber.hitNormal;
                            fiber.updatedNormalHeight[i] -= shiftRate;
                            fiber.vTimeBudgetSpent[i] += deltaTime;
                        }


                    }
                }
            }
        }

    }


    public void FlattenOp(RaycastHit hit, Ray ray, Vector3 hitNormal_MeshCon, float rayRM, float maxHalfHeight, float FlattenRateMult)
    {
        Vector3 vx;
        Vector3 TV;
        float raySqrDist, rayAlignedDistance;

        Vector3 A;
        Vector3 B;
        float x, y, z;

        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = hit.transform.InverseTransformDirection(ray.direction).normalized;
        List<IcingBuilder> oldIcings = PhaseManager.CurrentPhaseController.oldIcings;
        float shiftRate = settings.flatteningRate * Time.deltaTime * RotationSpeedManager.ChosenRPM * FlattenRateMult;
        float normalShiftRate = settings.flatteningRate * Time.deltaTime * RotationSpeedManager.ChosenRPM * 25;
        float deltaTime = Time.deltaTime;
        foreach (IcingBuilder peel in oldIcings)
        {
            foreach (IcingFibers fiber in peel.fibers)
            {
                for (int i = 0; i < fiber.vertices.Length; i++)
                {
                    vx = fiber.vertices[i];
                    TV.x = vx.x - rayOrigin.x;
                    TV.y = vx.y - rayOrigin.y;
                    TV.z = vx.z - rayOrigin.z;
                    A = rayDir;
                    B = TV;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    raySqrDist = x * x + y * y + z * z;
                    rayAlignedDistance = B.x * A.x + B.y * A.y + B.z * A.z;
                    if (rayAlignedDistance < 0) rayAlignedDistance = -rayAlignedDistance;


                    if ((raySqrDist < r_max * rayRM) && (rayAlignedDistance < maxHalfHeight))//(fiber.updatedNormalHeight[i] > -2*shiftRate)   && 
                    {
                        if (fiber.updatedNormalHeight[i] > 0 && fiber.edgeScaleFactor > 0 && fiber.vTimeBudgetSpent[i] <= IcingFibers.ITERATE_TIME_BUDGET)
                        {
                            fiber.normals[i] = Vector3.Lerp(fiber.normals[i], hitNormal_MeshCon, normalShiftRate * fiber.edgeScaleFactor).normalized;// (fiber.normals[i] + hitNormal*1.5f *Time.deltaTime).normalized;
                            //fiber.vertices[i] -= shiftRate * fiber.edgeScaleFactor * fiber.normals[i];// *fiber.centreOffsetMag[i];
                            fiber.vertices[i].x -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].x + fiber.hitNormal.x);
                            fiber.vertices[i].y -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].y + fiber.hitNormal.y);
                            fiber.vertices[i].z -= shiftRate * (fiber.edgeScaleFactor * fiber.normals[i].z + fiber.hitNormal.z);
                            fiber.updatedNormalHeight[i] -= shiftRate * fiber.edgeScaleFactor;// * fiber.centreOffsetMag[i];
                            peel.needsVisualUpdate = true;
                            fiber.vTimeBudgetSpent[i] += deltaTime;
                            //fiber.vertices[i] -= shiftRate * fiber.hitNormal;

                        }
                        else if (fiber.updatedNormalHeight[i] > -0.5f && fiber.vTimeBudgetSpent[i] <= IcingFibers.ITERATE_TIME_BUDGET)
                        {
                            fiber.vertices[i].x -= shiftRate * fiber.hitNormal.x;
                            fiber.vertices[i].y -= shiftRate * fiber.hitNormal.y;
                            fiber.vertices[i].z -= shiftRate * fiber.hitNormal.z;

                            //fiber.vertices[i] -= shiftRate * fiber.hitNormal;
                            fiber.updatedNormalHeight[i] -= shiftRate;
                            fiber.vTimeBudgetSpent[i] += deltaTime;
                        }


                    }
                }
            }
        }

    }


    public void FlattenOp2(RaycastHit hit, Ray ray, Vector3 hitNormal_MeshCon, float rayRM, float maxHalfHeight)
    {
        Vector3 vx;
        Vector3 TV;
        float raySqrDist, rayAlignedDistance;

        Vector3 A;
        Vector3 B;
        float x, y, z, tempVar;

        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = hit.transform.InverseTransformDirection(ray.direction).normalized;
        List<OldIceMesh> ancientIcing = (PhaseManager.CurrentPhaseController as PhaseController_Flattening).meshList;
        float shiftRate = settings.flatteningRate * Time.deltaTime * RotationSpeedManager.ChosenRPM;
        float normalShiftRate = settings.flatteningRate * Time.deltaTime * RotationSpeedManager.ChosenRPM * 25;
        for (int mi = 0; mi < ancientIcing.Count; mi++)
        {
            OldIceMesh oldIceMesh = ancientIcing[mi];
            if (oldIceMesh == null) continue;
            Vector3[] verts = oldIceMesh.vertices;
            Vector3[] norms = oldIceMesh.normals;
            for (int i = 0; i < verts.Length; i++)
            {
                vx = verts[i];
                TV.x = vx.x - rayOrigin.x;
                TV.y = vx.y - rayOrigin.y;
                TV.z = vx.z - rayOrigin.z;
                A = rayDir;
                B = TV;
                x = (A.y * B.z - A.z * B.y);
                y = (A.z * B.x - A.x * B.z);
                z = (A.x * B.y - A.y * B.x);
                raySqrDist = x * x + y * y + z * z;
                rayAlignedDistance = B.x * A.x + B.y * A.y + B.z * A.z;// Mathf.Abs();
                if (rayAlignedDistance < 0) rayAlignedDistance = -rayAlignedDistance;


                if ((raySqrDist < r_max * rayRM) && (rayAlignedDistance < maxHalfHeight))//(fiber.updatedNormalHeight[i] > -2*shiftRate)   && 
                {
                    tempVar = normalShiftRate;
                    x = norms[i].x * (1 - tempVar) + hitNormal_MeshCon.x * tempVar;
                    y = norms[i].y * (1 - tempVar) + hitNormal_MeshCon.y * tempVar;
                    z = norms[i].z * (1 - tempVar) + hitNormal_MeshCon.z * tempVar;

                    tempVar = Mathf.Sqrt(x * x + y * y + z * z);
                    norms[i].x = x / tempVar;
                    norms[i].y = y / tempVar;
                    norms[i].z = z / tempVar;
                    //norms[i] = norms[i]/tempVar ;// Vector3.Lerp(norms[i], hitNormal_MeshCon, normalShiftRate).normalized;// (fiber.normals[i] + hitNormal*1.5f *Time.deltaTime).normalized;
                    //verts[i] -= shiftRate* hitNormal_MeshCon;// *fiber.centreOffsetMag[i];
                    verts[i].x = verts[i].x - shiftRate * hitNormal_MeshCon.x;
                    verts[i].y = verts[i].y - shiftRate * hitNormal_MeshCon.y;
                    verts[i].z = verts[i].z - shiftRate * hitNormal_MeshCon.z;
                }
            }

            oldIceMesh.m.vertices = verts;
        }
    }
    /*
    public void FlattenOp_Rad(RaycastHit hit, Ray ray, Vector3 hitNormal, float flatteningRM, float targetRadius)
    {
        Vector3 vx;
        float currentDistance;
        float progressSum = 0;
        float totalCount = 0;
        Vector3 tempVec;
        float tempSqrDist;
        Vector3 A;
        Vector3 B;
        float x, y, z, maxRD;

        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = hit.transform.InverseTransformDirection(ray.direction).normalized;
        //Debug.Log(IcingMaker.oldPeels.Count);
        List<IcingBuilder> oldIcings = (PhaseManager.CurrentPhaseController as PhaseController_Flattening).icingPhaseRef.oldIcings;
        float  shiftRate = 0.15f * Time.deltaTime * RotationSpeedManager.ChosenRPM;
        foreach (IcingBuilder peel in oldIcings)
        {
            foreach (IcingFibers fiber in peel.fibers)
            {
                for (int i = 0; i < fiber.vertices.Length; i++)
                {
                    vx = fiber.vertices[i];
                    tempVec.x = vx.x - rayOrigin.x;
                    tempVec.y = vx.y - rayOrigin.y;
                    tempVec.z = vx.z - rayOrigin.z;
                    A = rayDir;
                    B = tempVec;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    tempSqrDist = x * x + y * y + z * z;// Vector3.Cross(rayDir, tempVec).sqrMagnitude;
                    //d = tempVec.x * tempVec.x + tempVec.y * tempVec.y + tempVec.z * tempVec.z;
                    if (tempSqrDist < r2 * flatteningRM)
                    {

                        fiber.normals[i] = Vector3.Lerp(fiber.normals[i], hitNormal, 2.5f * Time.deltaTime).normalized;// (fiber.normals[i] + hitNormal*1.5f *Time.deltaTime).normalized;
                        fiber.vertices[i] -= shiftRate *  fiber.normals[i];
                        peel.needsVisualUpdate = true;


                    }
                    if (fiber.initialRadialDistance[i] > targetRadius)
                    {

                        x = fiber.vertices[i].x;
                        z = fiber.vertices[i].z;
                        currentDistance = Mathf.Sqrt(x * x + z * z);
                        maxRD = (fiber.initialRadialDistance[i] - targetRadius);
                        totalCount += maxRD;
                        if (currentDistance > targetRadius)
                            progressSum += Mathf.Clamp(fiber.initialRadialDistance[i]-currentDistance,0,maxRD);
                        else
                            progressSum += maxRD;
                    }
                }
            }
        }


        progress = progressSum / totalCount;
    }


    public void FlattenOp_Top(RaycastHit hit, Ray ray, Vector3 hitNormal, float flatteningRM, float targetYHeight)
    {
        Vector3 vx;
        float currentHeight;
        float progressSum = 0;
        float totalCount = 0;
        Vector3 tempVec;
        float tempSqrDist;
        Vector3 A;
        Vector3 B;
        float x, y, z, maxYD;

        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = hit.transform.InverseTransformDirection(ray.direction).normalized;
        //Debug.Log(IcingMaker.oldPeels.Count);
        List<IcingBuilder> oldIcings = (PhaseManager.CurrentPhaseController as PhaseController_Flattening).icingPhaseRef.oldIcings;
        float shiftRate = 0.25f * Time.deltaTime * RotationSpeedManager.ChosenRPM;
        foreach (IcingBuilder peel in oldIcings)
        {
            foreach (IcingFibers fiber in peel.fibers)
            {
                for (int i = 0; i < fiber.vertices.Length; i++)
                {
                    vx = fiber.vertices[i];
                    tempVec.x = vx.x - rayOrigin.x;
                    tempVec.y = vx.y - rayOrigin.y;
                    tempVec.z = vx.z - rayOrigin.z;
                    A = rayDir;
                    B = tempVec;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    tempSqrDist = x * x + y * y + z * z;// Vector3.Cross(rayDir, tempVec).sqrMagnitude;
                    //d = tempVec.x * tempVec.x + tempVec.y * tempVec.y + tempVec.z * tempVec.z;
                    if (tempSqrDist < r2 * flatteningRM)
                    {

                        fiber.normals[i] = Vector3.Lerp(fiber.normals[i], hitNormal, 2.5f * Time.deltaTime).normalized;// (fiber.normals[i] + hitNormal*1.5f *Time.deltaTime).normalized;
                        fiber.vertices[i] -= shiftRate * Vector3.up ;
                        peel.needsVisualUpdate = true;


                    }
                    if (fiber.initialY[i] > targetYHeight)
                    {
                        currentHeight = fiber.vertices[i].y;// Mathf.Sqrt(x * x + z * z);
                        maxYD = (fiber.initialY[i] - targetYHeight);
                        totalCount += maxYD;

                        if (currentHeight > targetYHeight)
                            progressSum += Mathf.Clamp(fiber.initialY[i] - currentHeight, 0, maxYD);
                        else
                            progressSum += maxYD;
                    }
                }
            }
        }


        progress = progressSum / totalCount;
    }
    */
    #endregion


    private IcingMaker _icingMaker;
    public IcingMaker icingMaker
    {
        get
        {
            if (_icingMaker == null)
                _icingMaker = new IcingMaker(settings.icingSettings, trashRoot);
            return _icingMaker;
        }
    }

    public void OnToolUpdate(RaycastHit hit, Ray ray, bool isMain)
    {
        hit.point = hit.point + hit.normal * settings.extrusion;

        if (MainGameManager.settings.drawDebugRays) Debug.DrawRay(hit.point, hit.normal * 2, Color.yellow, 1);
        //float angle = Vector3.Angle(hit.normal, Vector3.up);


        Vector3 rchPoint = hit.transform.InverseTransformPoint(hit.point);

        //dirty = 0;
        //distCumulitive = 0;
        Vector3 travelVec;
        Vector3 travelOrigin;
        if (icingBuilder != null && icingBuilder.fibers.Count > 2)
        {
            travelOrigin = hit.transform.InverseTransformPoint(icingBuilder.root.TransformPoint(icingBuilder.lastFiber_2.basePoint - icingBuilder.lastFiber_2.upOffset.normalized * standardHalfPeelWidth * 100));
            travelVec = hit.transform.InverseTransformDirection(icingBuilder.root.TransformDirection(icingBuilder.lastFiber_1.basePoint - icingBuilder.lastFiber_2.basePoint).normalized).normalized;

        }
        else
        {
            travelVec = hit.transform.InverseTransformDirection(Vector3.Cross(hit.normal, Vector3.up)).normalized;
            Vector3 offsetDir = hit.transform.InverseTransformDirection(Vector3.Cross(hit.normal, travelVec)).normalized;
            travelOrigin = hit.transform.InverseTransformPoint(hit.point) - offsetDir * standardHalfPeelWidth * 1000;
        }
        closestSqrDist = float.MaxValue;
        farthestSqrDist = 0;
        uvUp = Vector2.zero;
        uvDown = Vector2.zero;

        Vector3 rOrigin = hit.point;
        rOrigin.y = 0;
        Vector3 rDir = Vector3.up;
        Ray verticalRay_hit_y0 = new Ray(rOrigin, rDir);
        Ray visualRay_hit = new Ray(hit.point, ray.direction);
        Vector3 hitNormal_mesh = hit.transform.InverseTransformDirection(hit.normal);

        //if (MainGameManager.settings.drawDebugRays)
        switch (PhaseManager.phase)
        {
            case GamePhase.AddCream:
                foreach (var item in morfMeshes)
                {
                    item.VertexPaintOperation_Sphere(rchPoint, travelVec, travelOrigin);
                }
                icingMaker.IcingUpdate(hit, uvUp, uvDown, settings.brushRadius);
                if (isMain && !MainGameManager.settings.editorPaintMode)
                {

                    FlattenOp_IP(
                        hit: hit, ray: visualRay_hit, hitNormal_MeshCon: hitNormal_mesh,
                        rayRM: 1f, maxHalfHeight: 0.125f, FlattenRateMult: 1);
                }
                break;

            case GamePhase.Flatten:
                PhaseController_Flattening pcf = PhaseManager.CurrentPhaseController as PhaseController_Flattening;
                if (isMain)
                {
                    if (pcf.meshList.Count > 0)
                    {
                        sampler_FlattenOp2.Begin();
                        FlattenOp2(hit, visualRay_hit, hitNormal_mesh, settings.defaultRadMult, settings.defaultTunnelHeight / 5);
                        sampler_FlattenOp2.End();
                    }
                    else
                    {
                        sampler_FlattenOp1.Begin();
                        FlattenOp(hit, visualRay_hit, hitNormal_mesh, settings.defaultRadMult, settings.defaultTunnelHeight / 5, 1);
                        sampler_FlattenOp1.End();
                    }
                    sampler_vShiftOpA.Begin();
                    Vector2 progress = new Vector2();
                    foreach (var item in morfMeshes)
                    {
                        Vector2 itemProgress = item.VertexShiftOperation_Mixed(hit, visualRay_hit, settings.defaultTunnelHeight);
                        progress += itemProgress;
                    }
                    this.progress = progress.x / progress.y;

                    sampler_vShiftOpA.End();

                }
                else
                {
                    sampler_vShiftOpB.Begin();
                    PhaseReferenceHolder prh = hit.collider.GetComponent<PhaseReferenceHolder>();
                    if (prh)
                    {
                        List<MorfMesh> meshes = prh.phaseRef.morfMeshes;
                        foreach (var item in meshes)
                        {
                            item.VertexShiftOperation_Mixed(hit, visualRay_hit, settings.defaultTunnelHeight);
                        }
                    }
                    else
                    {
                        Debug.Log(hit.collider.name);
                    }
                    sampler_vShiftOpB.End();
                }

                break;
            default:
            case GamePhase.Decorate:
                break;
        }
    }


    IcingBuilder icingBuilder { get { return icingMaker.icingBuilder; } }

    public bool IsIcing()
    {
        return icingBuilder != null;
    }

}

