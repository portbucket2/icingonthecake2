﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class PhaseController_Icing : PhaseController
{
    public override GamePhase phase { get { return GamePhase.AddCream; } }
    public override string ProgressText
    {
        get
        { 
            return "Add Icing!";
        }
    }
    public const int TEXMAP_RES = 512;

    public Color avgColor;

    [Header("Phase Manager Build Only")]
    [Range(0.98f,1)]
    public float defaultVertexCountToTargetConvertionRate = 0.99f;

    [Header("PhaseSpecific")]
    public bool vertexCountTaken = false;
    public int defaultColorIndex = -1;
    public int initialChoiceIndex = 0;
    public List<Color> cols;
    internal List<Material> mats = new List<Material>();

    public MorfMesh targetChoppedMesh;
    public Texture2D texRef;
    internal Color[][] texMap;
    internal Color[] vcolorTargets;

    public List<IcingBuilder> _oldIcings = new List<IcingBuilder>(150);

    private MeshFilter _targetMeshFilter;
    public MeshFilter targetMeshFilter
    {
        get
        {
            if (!_targetMeshFilter) _targetMeshFilter = targetChoppedMesh.GetComponent<MeshFilter>();
            return _targetMeshFilter;
        }
    }
    public override void OnPhaseStart()
    {
        //PhaseManager.instance.

        for (int i = 0; i < cols.Count; i++)
        {
            mats.Add(new Material(PhaseManager.instance.sampleMat));
            mats[i].color = cols[i];
        }

        base.OnPhaseStart();

        //PhaseManager.currentColChannelIndex = Random.Range(0, mats.Count);
    }


    public void CreateBodyDoubleForGallery(GalleryItemVcolorInfo vcolData )
    {
        Vector4[] colvecs = vcolData.colors;
        Color[] vcols = new Color[colvecs.Length];
        //Color map = new Color(0, 0, 0, 0);
        for (int i = 0; i < vcols.Length; i++)
        {
            vcols[i] = colvecs[i];
            //vcols[i].r = colvecs[i].x;
            //vcols[i].g = colvecs[i].y;
            //vcols[i].b = colvecs[i].z;
            //vcols[i].a = colvecs[i].w;


            //map += vcols[i];
        }
        //Color baseColor = new Color(vcolData.baseColor.x,vcolData.baseColor.y,vcolData.baseColor.z,vcolData.baseColor.w);
        MeshRenderer targetRend = targetChoppedMesh.GetComponent<MeshRenderer>();
        Material mat = targetRend.sharedMaterial;

        //Color R = new Color(0, 0, 0, 0), G = new Color(0, 0, 0, 0), B = new Color(0, 0, 0, 0), A = new Color(0, 0, 0, 0);

        //colList.Clear();
        //for (int i = 0; i < cols.Count; i++)
        //{
        //    switch (i)
        //    {
        //        case 0:
        //            R = cols[i];
        //            colList.Add(R, map.r);
        //            break;
        //        case 1:
        //            G = cols[i];
        //            colList.Add(G, map.g);
        //            break;
        //        case 2:
        //            B = cols[i];
        //            colList.Add(B, map.b);
        //            break;
        //        case 3:
        //            A = cols[i];
        //            colList.Add(A, map.a);
        //            break;
        //        default:
        //            break;
        //    }
        //    mat.SetColor(string.Format("_Color{0}", i + 1), cols[i]);
        //}



        MeshFilter bodyBig = meshRef.bodyBig;
        GameObject miniObj = Instantiate(bodyBig.gameObject, bodyBig.transform.position, bodyBig.transform.rotation, bodyBig.transform.parent.parent);
        miniObj.SetActive(true);
        Mesh m = miniObj.GetComponent<MeshFilter>().mesh;


        vcolorTargets = vcols;
        m.colors = vcols;


        MeshRenderer mrend = miniObj.GetComponent<MeshRenderer>();
        mrend.sharedMaterial = mat;
        for (int i = 0; i < cols.Count; i++)
        {
            mat.SetColor(string.Format("_Color{0}", i + 1), cols[i]);
        }

       
        //Color finalCol = colList.Roll();
        mat.SetColor("_Color0", vcolData.baseColor);
    }
    public Vector4[] GetColorForGalleryData()
    {
        Color[] cols = targetChoppedMesh.mesh.colors;
        Vector4[] colVecs = new Vector4[cols.Length];
        for (int i = 0; i < colVecs.Length; i++)
        {
            colVecs[i] = cols[i];
            //colVecs[i].x = cols[i].r;
            //colVecs[i].y = cols[i].g;
            //colVecs[i].z = cols[i].b;
            //colVecs[i].w = cols[i].a;
        }
        return colVecs;
    }
    private void Awake()
    {
      
        if (MainGameManager.settings.editorPaintMode) targetCount = targetCount * 2;
        //texRef = Resources.Load<Texture2D>(string.Format("TextureData/{0}", LevelPrefabManager.currentLevel.levelI + 1));


        if (texRef || fdata)
        {
            MeshFilter bodyBig = meshRef.bodyBig;
            GameObject miniObj = Instantiate(bodyBig.gameObject, bodyBig.transform.position, bodyBig.transform.rotation, bodyBig.transform.parent.parent);
           
            if(!MainGameManager.settings.enableSampleInMainView) miniObj.SetLayer("SecondaryOnly", true);
            miniObj.SetActive(true);
            Mesh m = miniObj.GetComponent<MeshFilter>().mesh;
            //MorfMesh cm = targetChoppedMesh;

            vcolorTargets = new Color[m.vertices.Length];
            Vector3[] vertices = m.vertices;
            Vector2[] uv = m.uv;
            if (texRef)
            {
                texMap = new Color[TEXMAP_RES][];
                for (int i = 0; i < TEXMAP_RES; i++)
                {
                    texMap[i] = new Color[TEXMAP_RES];
                }
                for (int i = 0; i < TEXMAP_RES; i++)
                {
                    for (int j = 0; j < TEXMAP_RES; j++)
                    {
                        texMap[i][j] = texRef.GetPixel(i, j);
                    }
                }
                for (int v = 0; v < vcolorTargets.Length; v++)
                {

                    Color col = texMap[Mathf.RoundToInt(TEXMAP_RES * uv[v].x)][Mathf.RoundToInt(TEXMAP_RES * uv[v].y)];
                    col.a = (1 - col.r)*(1 - col.g)*(1-col.b)*col.a;
                    vcolorTargets[v] = col;
                }
            }
            else if (fdata)
            {
                for (int i = 0; i < morfMeshes.Count; i++)
                {
                    if (morfMeshes[i] == targetChoppedMesh)
                    {
                        vcolorTargets = fdata.meshColKeeps[i].vcols;
                    }
                }

            }

            m.colors = vcolorTargets;
            m.vertices = vertices;

            MeshRenderer targetRend = targetChoppedMesh.GetComponent<MeshRenderer>();
            MeshRenderer mrend = miniObj.GetComponent<MeshRenderer>();
            Material mat = targetRend.sharedMaterial;
            mrend.sharedMaterial = mat;
            //Color R = new Color(0, 0, 0, 0), G = new Color(0, 0, 0, 0), B = new Color(0, 0, 0, 0), A = new Color(0, 0, 0, 0);
            for (int i = 0; i < cols.Count; i++)
            {
                mat.SetColor(string.Format("_Color{0}", i + 1), cols[i]);
            }
            //throw new System.Exception("I should handle this!");
        }

        //Color finalCol = colList.Roll();
        //mat.SetColor("_Color0", finalCol);

    }
    public float CalculateIcingMatch()
    {
        if (vcolorTargets == null) return 0.5f;
        float total = 0;
        float count = 0;
        float d ;
        Color col;
        //Debug.LogFormat("MaxI {0}, Cap {1}", targetChoppedMesh.vcolors.Length,vcolorTargets.Length);
        for (int i = 0; i < targetChoppedMesh.vcolors.Length; i++)
        {
            col = vcolorTargets[i];

            if (col.r > col.b && col.r > col.g && col.r > col.a)
            {

                d = (targetChoppedMesh.vcolors[i].r - vcolorTargets[i].r);
            }
            else if (col.g > col.b && col.g > col.a)
            {

                d = (targetChoppedMesh.vcolors[i].g - vcolorTargets[i].g);
            }
            else if (col.b > col.a)
            {

                d = (targetChoppedMesh.vcolors[i].b - vcolorTargets[i].b);
            }
            else
            {

                d = (targetChoppedMesh.vcolors[i].a - vcolorTargets[i].a);
            }
            if (d < 0) d = -d;
            total++;           

            if (d<0.5f)
            {
                count++;
            }
        }
        //Debug.Log(count / total);
        return count / total;

    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(PhaseController_Icing))]
public class PhaseController_IcingEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        PhaseController_Icing icingPhaseScript = (PhaseController_Icing)target;
        if (LevelPrefabManager.currentLevel)
        {
            string path = string.Format("Assets/Resources/PaintDump");//, LevelPrefabManager.currentLevel.levelI + 1);
            if (GUILayout.Button("Build Mesh", GUILayout.Height(50)))
            {

                FlattenPhaseDataLoader so = ScriptableObject.CreateInstance<FlattenPhaseDataLoader>();

                foreach (ColChannel cc in System.Enum.GetValues(typeof(ColChannel)))
                {
                    List<MeshFilter> meshFilters = new List<MeshFilter>();
                    for (int i = 0; i < icingPhaseScript.oldIcings.Count; i++)
                    {
                        if (icingPhaseScript.oldIcings[i].front == null) continue;
                        if (icingPhaseScript.oldIcings[i].colChannelIndex == (int)cc)
                        {
                            meshFilters.Add(icingPhaseScript.oldIcings[i].front.GetComponent<MeshFilter>());
                        }
                    }
                    CombineInstance[] combine = new CombineInstance[meshFilters.Count];

                    if (meshFilters.Count == 0)
                    {
                        //so.meshList.Add(null);
                        continue;
                    }

                    int j = 0;
                    while (j < meshFilters.Count)
                    {
                        combine[j].mesh = meshFilters[j].sharedMesh;
                        meshFilters[j].transform.position = Vector3.zero;
                        combine[j].transform = meshFilters[j].transform.localToWorldMatrix;
                        meshFilters[j].gameObject.SetActive(false);

                        j++;
                    }
                    Mesh m = new Mesh();
                    
                    m.CombineMeshes(combine);
                    string c;
                    switch (cc)
                    {
                        case ColChannel.R:
                            c = "R";
                            break;
                        case ColChannel.G:
                            c = "G";
                            break;
                        case ColChannel.B:
                            c = "B";
                            break;
                        case ColChannel.A:
                            c = "A";
                            break;
                        default:
                            c = "X";
                            break;
                    }
                    //if (!AssetDatabase.IsValidFolder(path))
                    //{
                    //    AssetDatabase.CreateFolder("Assets/Resources/PaintDump", string.Format("Level {0}", LevelPrefabManager.currentLevel.levelI + 1));
                    //}
                    AssetDatabase.CreateAsset(m, string.Format("{0}/p{1}_{2}.mesh", path, icingPhaseScript.phaseSetID, c));
                    AssetDatabase.SaveAssets();
                    //if (meshFilters.Count == 0)
                    //{
                    //    so.meshList.Add(m);
                    //    continue;
                    //}



                }
                so.meshColKeeps = new List<MeshCol>();
                for (int i = 0; i < PhaseManager.CurrentPhaseController.morfMeshes.Count; i++)
                {
                    MorfMesh cm = PhaseManager.CurrentPhaseController.morfMeshes[i];
                    so.meshColKeeps.Add(new MeshCol(cm.meshFilter.mesh.colors));
                }

                AssetDatabase.CreateAsset(so, string.Format("{0}/p{1}_fd.asset", path,icingPhaseScript.phaseSetID));
                AssetDatabase.SaveAssets();

            }
            else if (GUILayout.Button("Print Color", GUILayout.Height(50)))
            {
                Mesh m = icingPhaseScript.targetMeshFilter.mesh;
                Color[] vcols = m.colors;
                Vector2[] uv = m.uv;

                Texture2D tex2d = new Texture2D(PhaseController_Icing.TEXMAP_RES, PhaseController_Icing.TEXMAP_RES);
                for (int i = 0; i < vcols.Length; i++)
                {
                    Color col = new Color();
                    col.r = Mathf.Clamp01(vcols[i].r);
                    col.g = Mathf.Clamp01(vcols[i].g);
                    col.b = Mathf.Clamp01(vcols[i].b);
                    col.a = Mathf.Clamp01(vcols[i].a);

                    if (col.r > col.g)
                        col.g = 0;
                    else
                        col.r = 0;

                    if (col.r > col.b)
                        col.b = 0;
                    else
                        col.r = 0;

                    if (col.r > col.a)
                        col.a = 0;
                    else
                        col.r = 0;

                    if (col.g > col.b)
                        col.b = 0;
                    else
                        col.g = 0;

                    if (col.g > col.a)
                        col.a = 0;
                    else
                        col.g = 0;

                    if (col.b > col.a)
                        col.a = 0;
                    else
                        col.b = 0;


                    col.a = 1 - col.a;


                    tex2d.SetPixel(Mathf.RoundToInt(PhaseController_Icing.TEXMAP_RES * uv[i].x), Mathf.RoundToInt(PhaseController_Icing.TEXMAP_RES * uv[i].y), col);
                }
                tex2d.Apply();
                byte[] bytes = tex2d.EncodeToPNG();
                File.WriteAllBytes(string.Format("Assets/Resources/TextureData/{0}{1}.PNG", icingPhaseScript.phaseSetID, LevelPrefabManager.currentLevel.levelI + 1), bytes);
                //AssetDatabase.CreateAsset(tex2d, string.Format("{0}/builtTexture.PNG", path));
                //AssetDatabase.SaveAssets();
            }
        }

    }

}
#endif
