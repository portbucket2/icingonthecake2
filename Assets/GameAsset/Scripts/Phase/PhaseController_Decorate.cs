﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class PhaseController_Decorate : PhaseController
{
    public static event System.Action decorationComplete;
    //public GameObject decoratationPrefab;
    public Camera decoCam;
    //public RenderTexture rendTex;
    private void Awake()
    {
        decoCam.enabled = false;
        captureSaveCalled = false;
    }
    public override GamePhase phase{ get{ return GamePhase.Decorate; } }
    public override void SetCam(float val, float rate)
    {
        RotationSpeedManager.SetNewTargets(decoCam.transform, rate);
    }
    public override void OnPhaseStart()
    {
        //Debug.Log("<color='blue'>OnPhaseStart</color>");
        base.OnPhaseStart();
        decorationStarted = false;
        captureSaveCalled = false;
        //PhaseManager.OnGameplayComplete();
    }


    internal bool decorationStarted = false;
    float decorationStartTime;
    float decorationTime ;


    public void LoadDefaultDecoration(GalleryItemData decorationSaveData)
    {
        //Debug.Log("<color='yellow'>CSV Based Decoration Start Called</color>");
        this.decoID = LevelPrefabManager.currentLevel.levelDefinition.decorationID;
        this.variant = LevelPrefabManager.currentLevel.levelDefinition.decorationColorVariantID;
        StartDecoration(MainGameManager.settings.decorationSharedMaterial, decorationSaveData);
    }
    [SerializeField] string decoID;
    [SerializeField] string variant;
    [SerializeField] GameObject decoItem;
    public void LoadCustomDecoration(string decoID, string variant, bool loadWithSharedMat)
    {
        this.decoID = decoID;
        this.variant = variant;
        StartDecoration(loadWithSharedMat,null);
    }
    public void StartDecoration(bool loadWithSharedMat , GalleryItemData decorationSaveData)
    {
        if (decoItem) Destroy(decoItem);
        decorationStarted = true;
        decorationStartTime = Time.time;
        GameObject prefab = Resources.Load<GameObject>(LevelPrefabManager.currentLevel.levelDefinition.GetAssetPathForDecoID(decoID));
        if (prefab)
        {
            decoItem = Instantiate(prefab, PhaseManager.instance.transform);
            DecoratePrefabController decoCon = decoItem.GetComponent<DecoratePrefabController>();
            decoCon.Initiate(variant,decoID, loadWithSharedMat);
            decorationTime = decoCon.StartAnimate(() =>
            {
                decorationComplete?.Invoke();
                if(decorationSaveData!=null) decoCamRoutine= StartCoroutine(DecoCamRoutine(decorationSaveData));
            });
        }
        else
        {
            Debug.LogErrorFormat("decoration Not found {0}", LevelPrefabManager.currentLevel.levelDefinition.GetAssetPathForDecoID(decoID));
        }
    }

    Coroutine decoCamRoutine;
    IEnumerator DecoCamRoutine(GalleryItemData gidToSaveCapture)
    {
        //Debug.LogFormat("<color='yellow'>deco cam enabled</color>");
        Transform rotObj = RotationSpeedManager.instance.rotationTarget;
        GalleryManager.instance.SaveGalleryImage(decoCam, gidToSaveCapture);
        while (true)
        {
            Vector3 eulAngle = rotObj.localRotation.eulerAngles;
            if (eulAngle.y > -20 && eulAngle.y < 40)
            {
                break;
            }
            yield return null;
        }
        //Debug.LogFormat("<color='yellow'>deco cam disabled</color>");
        GalleryManager.instance.SaveGalleryImage(decoCam, gidToSaveCapture);
    }

    bool captureSaveCalled = false;


#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadCustomDecoration(
LevelPrefabManager.currentLevel.levelDefinition.decorationID,
    LevelPrefabManager.currentLevel.levelDefinition.decorationColorVariantID, true);
        }
    }
#endif


    public override float CalculateProgress()
    {
        if (!decorationStarted) return -1;

        return Mathf.Clamp01((Time.time - decorationStartTime) / decorationTime);
    }
    public override string ProgressText
    {
        get
        {
            if (PhaseManager.allPhaseOver)
                return "Perfect!";
            else 
                return "Decorating...";
            //float prog = PhaseManager.CurrentPhaseController.CalculateProgress();
            //if (prog > 0)
            //{
            //    return "Decorating...";
            //}
            //else
            //{
            //    return "Nice Work!";
            //}
            //switch (PhaseManager.phase)
            //{
            //    case GamePhase.AddCream:
            //        return "Add Icing!";
            //    case GamePhase.Flatten:
            //        return "Smooth It Out";
            //    case GamePhase.DropPatty:
            //        return "Tap to Drop!";
            //    case GamePhase.Decorate:
                    
            //    default:
            //        return "";
            //}
        }
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(PhaseController_Decorate))]
public class PhaseController_Decorate_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        PhaseController_Decorate decoPhase = (PhaseController_Decorate)target;
        if (GUILayout.Button("Load Deco", GUILayout.Height(50)))
        {
            decoPhase.StartDecoration(true,null);
        }
        if (GUILayout.Button("Load Default Deco", GUILayout.Height(50)))
        {
            decoPhase.LoadCustomDecoration(
LevelPrefabManager.currentLevel.levelDefinition.decorationID,
    LevelPrefabManager.currentLevel.levelDefinition.decorationColorVariantID, true);
        }
    }
}
#endif