﻿using System.Collections.Generic;
using FRIA;
using UnityEngine;
using UnityEngine.Events;

public class PhaseController : MonoBehaviour
{

    public static event System.Action<PhaseController> onPhaseStart_StaticEvent;
    public virtual GamePhase phase
    {
        get
        {
            Debug.LogError("Needs ovverride!");
            return 0;
        }
    }
    [Range(0, 1)]
    public float nextPhasePromptLocation = 1;
    public int phaseSetID=-1;
    [Range(0, 1)]
    public float phaseCamDefaultPos = 0;
    public OperationSettings operationSettings;
    public List<MorfMesh> morfMeshes;
    public MeshCollider phaseCollider;
    public MeshRefKeep meshRef;
    public float targetCount = 1;

    public UnityEvent onPhaseStart;

    private FlattenPhaseDataLoader _fdata;
    public FlattenPhaseDataLoader fdata
    {
        get
        {
            if (!_fdata)
            {
                _fdata = Resources.Load<FlattenPhaseDataLoader>(string.Format("{0}/p{1}_fd", LevelPrefabManager.currentLevel.levelDefinition.resourcePath_dataFolder,phaseSetID));

                //Debug.Log(_fdata);
            }
            return _fdata;
        }
    }

    internal PhaseOperation opCon;

    public virtual void OnPhaseStart()
    {
        if (phase != GamePhase.Decorate)
        {
            opCon = new PhaseOperation(operationSettings, this.transform);
            foreach (var item in morfMeshes)
            {
                item.OnPhaseStart(opCon, phase);
            }
        }
        onPhaseStart.Invoke();
        onPhaseStart_StaticEvent?.Invoke(this);

        if (phase == GamePhase.Flatten && !(this as PhaseController_Flattening).icingPhaseRef.enabled)
        {
            SetCam(1, 10);
        }
        else
        {
            SetCam(phaseCamDefaultPos, 10);
        }
        switch (phase)
        {
            default:
            case GamePhase.AddCream:
            case GamePhase.Flatten:
                MiniCamController.SetState(true);
                break;
            case GamePhase.DropPatty:
            case GamePhase.Decorate:
                MiniCamController.SetState(false);
                break;
        }
        //MiniCamController.SetState(phase != GamePhase.DropPatty);
    }

    public virtual void OnPhaseEnd()
    {
        MiniCamController.SetState(false);
    }

    public virtual void SetCam(float val, float rate)
    {
        RotationSpeedManager.SetNewTargets(val,rate);
    }



    public List<IcingBuilder> oldIcings
    {
        get
        {
            switch (phase)
            {
                case GamePhase.AddCream:
                    return (this as PhaseController_Icing)._oldIcings;
                case GamePhase.Flatten:
                    return (this as PhaseController_Flattening).icingPhaseRef._oldIcings;
                default:
                    return null;
            }
        }
    }
    public void Update()
    {
        //int updateCount = 0;
        if (oldIcings != null)
        {
            foreach (var item in oldIcings)
            {
                if (item.needsVisualUpdate)
                {
                    //updateCount++;
                    item.UpdateFiberMesh(true);
                }
            }
        }
        //Debug.Log(updateCount);



    }
    protected ChancedList<Color> colList = new ChancedList<Color>();

    public void EditorInit(float divs)
    {
        for (int i = 0; i < morfMeshes.Count; i++)
        {
            morfMeshes[i].RegisterVertices(divs);
            morfMeshes[i].PrintVCount();
            morfMeshes[i].isPrimaryMesh = (i == 0);
        }
        if (phaseCollider)
        {
            PhaseReferenceHolder prh = phaseCollider.GetComponent<PhaseReferenceHolder>();
            if (!prh) prh = phaseCollider.gameObject.AddComponent<PhaseReferenceHolder>();
            prh.phaseRef = this;
        }

    }


    public virtual float CalculateProgress()
    {
        return opCon.progress / targetCount;
    }
    public virtual string ProgressText
    {
        get
        {
            if (PhaseManager.allPhaseOver)
                return "Perfect!";
            else
                return "Tap and Hold";
        }
    }
}
