﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshRefKeep : MonoBehaviour
{
    public MeshFilter bodySmall;
    public MeshFilter bodyBig;
    public MeshFilter colliderSmall;
    public MeshFilter colliderBig;
    public GameObject dummy;
}
