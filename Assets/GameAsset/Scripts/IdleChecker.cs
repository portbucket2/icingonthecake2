﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleChecker : MonoBehaviour
{
    public static IdleChecker instance;

    public float idletime = 60;
    public float tm = 0;
    public float swipefeedbacktime = 1;
    //public float tapfeedbacktime = 1;

    //public GameObject mainidle;
    public GameObject gameidle;
    public GameObject text;

    public bool allowPeriodicHand = true;
    // Start is called before the first frame update
    void Start ()
    {
        if ( instance == null )
        {
            instance = this;
            DontDestroyOnLoad ( gameObject );
        }
        else
        {
            DestroyImmediate ( this );
        }

        if ( LevelLoader.Last_li == 0 && LevelLoader.Last_ai == 0 )
        {
            Invoke ( "showhandnow", 1 );
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetMouseButton(0)
            || GalleryManager.instance.galleryRootObject.activeInHierarchy
            || SmoothToolCanvasManager.Instance.panelRoot.activeInHierarchy
            || GameProgressManager.isVipLevel
            )
        {
            AppLovinCrossPromo.Instance().HideMRec();
            tm = 0;
            text.SetActive ( false );
            gameidle.SetActive ( false );
        }
        else
        {
            tm += Time.deltaTime;
        }

        if ( tm > idletime )
        {
            tm = 0;

            if (PhaseManager.instance && !PhaseManager.allGameplayOver )
            {
                if(allowPeriodicHand)StartCoroutine ( runidlefeedback () );
            }

            
        } 
    }

    IEnumerator runidlefeedback ( )
    {
        gameidle  .SetActive ( true );
        text.SetActive ( true );

        yield return new WaitForSeconds (  swipefeedbacktime );

        gameidle  .SetActive ( false );
        text.SetActive ( false );
    }

    public void resetEverything ()
    {
        tm = 0;
        gameidle.SetActive ( false );
        text.SetActive ( false );
        allowPeriodicHand = true;
    }

    public void showhandnow (  )
    {
        StartCoroutine ( runidlefeedback (  ) );
    }



}
