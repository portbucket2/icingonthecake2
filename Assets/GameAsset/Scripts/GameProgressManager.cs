﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameProgressManager : MonoBehaviour
{
    public static bool isVipLevel;
    public static GameProgressManager instance;
    float topLimit = 612;
    float botLimit = 17;
    private int _starCount = 0;
    public int StarCount
    {
        get { return _starCount; }
        set
        {
            if (value >= 3 && _starCount < 3)
            {
                star3.SetTrigger("go");
            }
            if (value >= 2 && _starCount < 2)
            {
                star2.SetTrigger("go");
            }
            if (value >= 1 && _starCount < 1)
            {
                star1.SetTrigger("go");
            }
            else if (value == 0)
            {
                star1.ResetTrigger("go");
                star2.ResetTrigger("go");
                star3.ResetTrigger("go");

                star1.SetTrigger("reset");
                star2.SetTrigger("reset");
                star3.SetTrigger("reset");
            }
            _starCount = value;
        }
    }

    public GameObject rootObject;

    public Image progressSlider;
    public Text progressRelatedText;
    public Animator progressRelatedTextAnim;




   // public RectTransform pbar;
    public RectTransform m1;
    public RectTransform m2;
    public RectTransform m3;
    public Animator star1;
    public Animator star2;
    public Animator star3;
    public GameObject uiDisableOnWinItem_early;
    public GameObject uiDisableOnWinItem;

    public Button phaseContinueButton;
    public Text continueText;

    //public GameObject confetti;
    public ParticleSystem confettiSystem1;
    public ParticleSystem confettiSystem2;
    public ParticleSystem[] confettiChristmas;

    public GameObject collidePrefab;

    public GameObject vipLevelObject;
   

    private void Awake()
    {
        instance = this;
        LevelPrefabManager.newPrefabLoading += OnNewPrefabLoad;
        phaseContinueButton.onClick.AddListener(OnPhaseContinueButton);
        PhaseManager.onPhaseEdges += OnPhaseEdge;
        PhaseController.onPhaseStart_StaticEvent += OnAnyPhaseStart;
    }
    bool isPhaseShceduledToTerminate;
    void OnAnyPhaseStart(PhaseController pc)
    {
        rootObject.SetActive(LevelPrefabManager.currentLevel.loadType != LoadType.GALLERY) ;
        isPhaseShceduledToTerminate = false;
    }
    private void OnPhaseEdge()
    {
        progressRelatedTextAnim.SetTrigger("shake");
    }
    private void OnDestroy()
    {
        PhaseController.onPhaseStart_StaticEvent -= OnAnyPhaseStart;
        phaseContinueButton.onClick.RemoveListener(OnPhaseContinueButton);
        LevelPrefabManager.newPrefabLoading -= OnNewPrefabLoad;
        PhaseManager.onPhaseEdges -= OnPhaseEdge;
    }

    public void DoCompletionCelebration()
    {
        confettiSystem1.Play();
        confettiSystem2.Play();
        foreach ( ParticleSystem p in confettiChristmas )
        {
            p.Play ();
        }
        if (hapticSoundSource.hapticEnabled)
        {
            Handheld.Vibrate();
            //Debug.Log("VIBRATTTTIIIINNNGGGGG");
        }
    }
    void OnPhaseContinueButton()
    {
        AnalyticsAssistant.SwitchedEarlyToSpatula(LevelPrefabManager.currentLevel.cumulitiveLevelNo);
        DemandPhaseAdvance();
    }
    public void DemandPhaseAdvance()
    {
        phaseAdvancementPrompted = false;
        phaseContinueButton.gameObject.SetActive(false);
        IcingMaker.EndCurrentIcing("Phase Over");
        PhaseManager.instance.MoveToNextPhase();
    }

    internal bool phaseAdvancementPrompted;
    public void PromptPhaseAdvancement(string text)
    {
        if (phaseAdvancementPrompted) return;

        phaseAdvancementPrompted = true;
        phaseContinueButton.gameObject.SetActive(true);
    }
    private void OnNewPrefabLoad(Theme theme)
    {
        StarCount = 0;
        phaseAdvancementPrompted = false;
        phaseContinueButton.gameObject.SetActive(false);
        PhaseManager.instance.forceUseDefaultCamTrans = false;
        progressSlider.fillAmount = 0;
        uiDisableOnWinItem.SetActive(true);
        RotationSpeedManager.instance.BreakRotation(0.75f, true);
        isVipLevel = SuccessBoxController.ConsumeVIPToken();
        vipLevelObject.SetActive(isVipLevel);
    }
    void Update()
    {
        float progress = PhaseManager.CurrentPhaseController.CalculateProgress();
        bool enoughToGoToNextPhase = progress >= 1;
        switch (PhaseManager.phase)
        {
            case GamePhase.AddCream:
                {
                    progressSlider.fillAmount = progress;
                    if (progress > PhaseManager.CurrentPhaseController.nextPhasePromptLocation &&  (!phaseAdvancementPrompted))
                    {
                        PromptPhaseAdvancement("");
                    }
                }
                break;
            case GamePhase.Flatten:
                {
                    progressSlider.fillAmount = progress;
                    if (enoughToGoToNextPhase && !isPhaseShceduledToTerminate)
                    {
                        isPhaseShceduledToTerminate = true;
                        PhaseManager.FlattenPhaseTemporalCompletion();
                    }
                    enoughToGoToNextPhase = false;
                }
                break;
            case GamePhase.DropPatty:
                {
                    progressSlider.fillAmount = progress;
                }
                break;
            case GamePhase.Decorate:
                {
                    if ((PhaseManager.CurrentPhaseController as PhaseController_Decorate).decorationStarted)
                    {
                        progressSlider.fillAmount = progress;
                    }
                }
                break;
            default:
                Debug.LogError("Unexpected case");
                break;

        }

        progressRelatedText.text = PhaseManager.CurrentPhaseController.ProgressText;

        if (!PhaseManager.allPhaseOver && enoughToGoToNextPhase)
        {
            DemandPhaseAdvance();
            return;
        }
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.End))
        {
            DemandPhaseAdvance();
            return;
        }
#endif



        StarCount = PhaseManager.GetProgressStarCount();
    }




}