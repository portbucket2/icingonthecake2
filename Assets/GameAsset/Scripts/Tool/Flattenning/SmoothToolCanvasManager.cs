﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class SmoothToolCanvasManager : MonoBehaviour
{
    public static SmoothToolCanvasManager Instance { get; private set; }
    public static event System.Action<SmoothToolProfile> onNewToolChosen;

    public GameObject panelRoot;
    public SmoothToolProfileManager profileMan;
    public GameObject uiItemPrefab;
    public Transform contentRoot;
    [Header("Monitor only")]
    public List<SmoothToolItemUI> UIItemList;


    private void Awake()
    {
        Instance = this;
        profileRandomizer = new ChancedList<SmoothToolProfile>();
    }

    FRIA.ChancedList<SmoothToolProfile> profileRandomizer;
    public SmoothToolProfile NextProfile()
    {
        profileRandomizer.Clear();
        foreach (SmoothToolProfile profile in profileMan.profiles)
        {
            if (profile.IsUnlocked)
            {
                continue;
            }
            else profileRandomizer.Add(profile, 1);
        }
        if (profileRandomizer.items.Count > 0)
        {
            return profileRandomizer.Roll();
        }
        else
        {
            return null;
        }
    }

    public void LoadScreen()
    {
        panelRoot.SetActive(true);
        for (int i = UIItemList.Count - 1; i >= 0; i--)
        {
            Pool.Destroy(UIItemList[i].gameObject);
            UIItemList.RemoveAt(i);
        }
        for (int i = 0; i < profileMan.profiles.Count; i++)
        {
            SmoothToolItemUI UIItem = Pool.Instantiate(uiItemPrefab, contentRoot).GetComponent<SmoothToolItemUI>();
            UIItem.transform.localScale = Vector3.one;
            UIItemList.Add(UIItem);
            int index = i;
            SmoothToolProfile tp = profileMan.profiles[index];
            UIItem.Load(tp,tp.active_index ==  SmoothToolVisualController.GetChosenIndex(), OnSelection);

        }
    }

    public void OnSelection(SmoothToolProfile profile)
    {
        onNewToolChosen?.Invoke(profile);
        panelRoot.SetActive(false);
    }
}
