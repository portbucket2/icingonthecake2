﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class SmoothToolVisualController : MonoBehaviour
{
    public static SmoothToolVisualController Instance { get; private set; }

    public static HardData<int> currentChosenToolHD;
    private void Awake()
    {
        Instance = this;
        if (currentChosenToolHD == null) currentChosenToolHD = new HardData<int>("CURRENT_SMOOTH_TOOLCHOICE", 0);

        SmoothToolCanvasManager.onNewToolChosen += OnToolChanged;
    }

    void OnToolChanged(SmoothToolProfile profile)
    {
        currentChosenToolHD.value = profile.active_index;  
    }
    private void OnDestroy()
    {
        SmoothToolCanvasManager.onNewToolChosen -= OnToolChanged;
    }


    public static int GetChosenIndex()
    {
        if (!Instance) throw new System.Exception("Unexpected");
        return currentChosenToolHD.value;
    }

    private void Start()
    {
        SetCurrentVisual();
    }

    //public List<GameObject> visualList = new List<GameObject>();


    public bool shouldBeVisible = false;
    public void SetVisibility(bool active)
    {
        shouldBeVisible = active;
        SetCurrentVisual();
    }

    public Transform rootItem;
    public void SetCurrentVisual()
    {
        int i=0;
        foreach (Transform item in rootItem)
        {
            bool acitve = shouldBeVisible && (i == GetChosenIndex());
            item.gameObject.SetActive(acitve);
            i++;
            
        }
    }
}
