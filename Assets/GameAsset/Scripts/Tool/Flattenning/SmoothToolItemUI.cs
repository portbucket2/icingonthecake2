﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.Ad;


public class SmoothToolItemUI : MonoBehaviour
{
    public GameObject mainObject;
    public Text toolName;
    public Image profilePicImage;
    public Button selectButton;
    public GameObject isSelectedObject;
    public GameObject isLockedObject;
    public Image bgImage;
    public Color selectedColor;
    public Color normalColor;
    public Color lockedColor;


    public void Load(SmoothToolProfile profile, bool isSelected, System.Action<SmoothToolProfile> onNewToolChosen)
    {

        mainObject.SetActive(true);
        bool isUnlocked = profile.IsUnlocked;
        profilePicImage.sprite = profile.proPic;

        toolName.text = isUnlocked ? profile.title : "";

        bgImage.color = isUnlocked?( isSelected ? selectedColor : normalColor) : lockedColor;


        isLockedObject.SetActive(!isUnlocked);
        isSelectedObject.SetActive(isSelected);

        //if (Application.isEditor)
        //{
        //    selectButton.interactable = true;// profile.IsUnlocked;
        //}
        //else
        //{
        //    selectButton.interactable = profile.IsUnlocked;
        //}


        selectButton.onClick.RemoveAllListeners();


        selectButton.onClick.AddListener(() =>
        {
            if (profile.IsUnlocked)
            {
                onNewToolChosen?.Invoke(profile);
            }
            else
            {
                if (Application.isEditor && !AdController.instance)
                {
                    profile.Unlock();
                    onNewToolChosen?.Invoke(profile);
                }
                else
                {
                    AdController.ShowRewardedVideoAd((bool success) => {

                        if (success)
                        {
                            profile.Unlock();
                            onNewToolChosen?.Invoke(profile);
                        }
                    });
                }

            }
        });
        //selectButton.onClick.AddListener(() =>
        //{
        //    if (isUnlocked)
        //    {
        //        ToolSelectionManager.mode = profile.type;
        //        onNewToolChosen?.Invoke();
        //    }
        //    else switch (profile.unlockType)
        //        {
        //            case UnlockType.STAR:
        //                {

        //                    if (StarGazer.StarsAvailable < profile.directCostRemainingToUnlock)
        //                    {
        //                        DialogueAcceptReject.instance.Load_1Choice("Not enough stars!", "Finishing levels quickly will unlock more stars!", null);
        //                    }
        //                    else
        //                    {
        //                        //Debug.LogError("DialogureCalled");
        //                        DialogueAcceptReject.instance.Load_2Choice(
        //                            titleText: "Unlock Tool",
        //                            bodyText: string.Format("Unlock this item for {0} stars?", profile.directCostRemainingToUnlock.value),
        //                            onAccept: () =>
        //                            {
        //                                StarGazer.ConsumeStars(profile.directCostRemainingToUnlock);
        //                                profile.ReduceRemainingCost(profile.directCostRemainingToUnlock);
        //                                ToolSelectionManager.mode = profile.type;
        //                                onNewToolChosen?.Invoke();

        //                            });
        //                    }

        //                }
        //                break;
        //            case UnlockType.VIDEO:
        //                {
        //                    if (Application.isEditor)
        //                    {
        //                        profile.ReduceRemainingCost(1);
        //                        if (profile.directCostRemainingToUnlock > 0)
        //                        {
        //                            Load(profile, isSelected, onNewToolChosen);
        //                        }
        //                        else
        //                        {
        //                            ToolSelectionManager.mode = profile.type;
        //                            onNewToolChosen?.Invoke();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        selectButton.interactable = false;
        //                        Portbliss.Ad.AdController.ShowRewardedVideoAd((bool success) => {
        //                            if (success)
        //                            {
        //                                profile.ReduceRemainingCost(1);
        //                                if (profile.directCostRemainingToUnlock > 0)
        //                                {
        //                                    Load(profile, isSelected, onNewToolChosen);
        //                                }
        //                                else
        //                                {
        //                                    ToolSelectionManager.mode = profile.type;
        //                                    onNewToolChosen?.Invoke();
        //                                }
        //                            }
        //                            else
        //                            {
        //                                selectButton.interactable = true;
        //                            }

        //                        });
        //                    }

        //                }
        //                break;
        //        }

        //});
    }
}
