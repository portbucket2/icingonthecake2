﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
[CreateAssetMenu(fileName = "SmoothToolProfileManager", menuName = "Icing/SmootherProfileMan", order = 1)]
public class SmoothToolProfileManager : ScriptableObject
{
    public List<SmoothToolProfile> profiles;
}
#if UNITY_EDITOR

[CustomEditor(typeof(SmoothToolProfileManager))]
public class SmoothToolProfilesEditor : Editor
{
    public static SmoothToolProfileManager currentSelection;
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"));
        EditorGUI.EndDisabledGroup();
        currentSelection = (SmoothToolProfileManager)base.target;
        serializedObject.Update();
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("title"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("displaySprite"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("displaySprite_seleceted"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("defaultIsEmpty"));
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("defaultItem"));
        Show(serializedObject.FindProperty("profiles"), currentSelection.profiles);
        serializedObject.ApplyModifiedProperties();
    }

    public static void Show(SerializedProperty list, List<SmoothToolProfile> listRef)
    {
        //List<SkinItemProfile> listRef = sst.tabContents;
        EditorGUILayout.Space();
        EditorGUI.indentLevel += 1;

        GUILayout.BeginVertical("Box");

        //List<int> duplicateCheckList = new List<int>();
        for (int i = 0; i < list.arraySize; i++)
        {
            //SkinItemProfile sp = splu.listup[i];
            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("", GUILayout.Width(100));
            float defWidth = 70;
            float defHeight = 20;
            if (GUILayout.Button("- Remove", GUILayout.Width(defWidth), GUILayout.Height(defHeight)))
            {
                listRef.RemoveAt(i);
            }
            if (GUILayout.Button("+ Add", GUILayout.Width(defWidth), GUILayout.Height(defHeight)))
            {
                listRef.Insert(i + 1, new SmoothToolProfile(i + 1));
            }

            EditorGUILayout.LabelField("", GUILayout.MinWidth(0));
            //EditorGUILayout.LabelField("");
            if (GUILayout.Button("↑", GUILayout.Width(defHeight), GUILayout.Height(defHeight)))
            {
                if (i != 0)
                {
                    SmoothToolProfile ld = listRef[i];
                    listRef[i] = listRef[i - 1];
                    listRef[i - 1] = ld;

                }
            }

            if (GUILayout.Button("↓", GUILayout.Width(defHeight), GUILayout.Height(defHeight)))
            {
                if (i != list.arraySize - 1)
                {
                    SmoothToolProfile ld = listRef[i];
                    listRef[i] = listRef[i + 1];
                    listRef[i + 1] = ld;
                }
            }

            GUILayout.EndHorizontal();
        }

        if (listRef.Count == 0)
            if (GUILayout.Button("(+)Add"))
            {
                listRef.Insert(listRef.Count, new SmoothToolProfile(0));
            }
        EditorGUILayout.EndVertical();

        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Space();


        //if (GUILayout.Button("(+)Add"))
        //{
        //    scinfo.dataList.Add(new CSVDownloadData());
        //}
    }
}
#endif