﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ButtonLinkConnector : MonoBehaviour
{
    public List<ButtonLinkUp> linkups;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var item in linkups)
        {
            item.Init();
        }
    }

    [System.Serializable]
    public class ButtonLinkUp
    {
        public Button button;
        public string link;

        public void Init()
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener( ()=>Application.OpenURL(link));
        }
    }
}