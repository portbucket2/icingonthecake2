﻿
using UnityEngine;
[System.Serializable]

public class OperationSettings :ScriptableObject
{
    public IcingSettings icingSettings;
    public float flatteningRate = 1.5f;
    public float brushRadius = 0.08f;
    public float extrusion = -0.02f;
    public float normalPushOutValue = .1f;
    public float defaultTunnelHeight = .2f;
    public float defaultRadMult = 5f;
    public float targetRadius = 0.6f;
    public float targetYHeight = 0.675f;
    [Range(0, 1)]
    public float primaryInnerRad = .736f;

    [Range(0, 1)]
    public float secondaryInnerRad = .736f;
}
