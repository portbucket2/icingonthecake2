﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Analytics;
using FRIA;
using com.adjust.sdk;

public static class AnalyticsAssistant 
{

#if UNITY_EDITOR
    static bool logToConsole = false;
#else
    static bool logToConsole = false;
#endif

    public static void LevelStarted(int levelNo, string gameplay, int layerCount)
    {
        if (logToConsole) Debug.LogFormat("Started Level {0}, gameplay: {1}, height {2}", levelNo, gameplay,layerCount);
        AnalyticsController.LogLevelStarted(levelNo, gameplay, layerCount);
    }
    public static void LevelCompleted(int levelNo, int stars, string gameplay, int layerCount)
    {
        if (logToConsole) Debug.LogFormat("Completed Level {0} with stars {3}, gameplay: {1}, height {2}", levelNo, gameplay, layerCount, stars);
        AnalyticsController.LogLevelCompleted(levelNo,stars,gameplay,layerCount);
    }
    public static void LevelRestarted(int levelNo, string gameplay, int layerCount)
    {
        if (logToConsole) Debug.LogFormat("Restartd Level {0}, gameplay: {1}, height {2}", levelNo, gameplay, layerCount);
        AnalyticsController.LogLevelReStarted(levelNo, gameplay, layerCount);
        
    }
    public static void SwitchedEarlyToSpatula(int levelNo)
    {
        if (logToConsole) Debug.LogFormat("Switched To Spatula manually at level no: {0}", levelNo);
        AnalyticsController.LogToolSwitchedTo(levelNo,"Spatula");
    }
    public static void SpeedChanged()
    {
        if (logToConsole) Debug.LogFormat("Speed was changed");
        AnalyticsController.LogSpeedChanged();
    }
    public static void LogABTesting(string abType, string abValue)
    {
        Debug.Log("<color='green'>AB value choice made type:" + abType + " and ab value: " + abValue+" in Analytics assistant</color>");
        AnalyticsController.LogABTesting(abType, abValue);
    }

    public static void LevelCompletedAppsFlyerIfItIsProperLevel(int lvNow)
    {
        List<int> lvList = GameConfig.levelCompletionAppsflyerOneTimeHD.AllNumbers;
        if (lvNow >= 1)
        {
            foreach (var v in lvList)
            {
                HardData<bool> hd = GameConfig.levelCompletionAppsflyerOneTimeHD.GetConfigForLevel(v); //new HardData<bool>("LEVEL_COMPLETED_ONE_TIME_EVENT_FIRED_" + v, false);
                if (hd.value == false && lvNow >= v)
                {
                    Debug.Log("<color='magenta'>level achieved num: " + v + "</color>");

                    string evToken_levelCompleted = GameConfig.levelCompletedAdjustEvents[v];
                    AdjustEvent adjustEvent = new AdjustEvent(evToken_levelCompleted);
                    Adjust.trackEvent(adjustEvent);
                    hd.value = true;
                }
            }
        }
    }
     
    /*
    public static void LevelSkipped(int levelNo, string levelName, string levelType)
    {
        HardData<bool> hd = new HardData<bool>("Ad_SKIPPED_ONE_TIME_EVENT_FIRED", false);
        if (hd.value == false)
        {
            string evToken_levSkipped = "INSERT";
#if UNITY_ANDROID
                        evToken_levSkipped = "INSERT";
#elif UNITY_IOS
            evToken_levSkipped = "INSERT";
#endif
            AdjustEvent adjustEvent = new AdjustEvent(evToken_levSkipped);
            Adjust.trackEvent(adjustEvent);


            //AppsFlyer.trackRichEvent("level_skipped", new Dictionary<string, string>());
            Debug.Log("<color='magenta'>level skip analytics method</color>");
            hd.value = true;
        }
    }
    */

    public static void LogRewardedVideoAdStart(int levelnumber)
    {
        Debug.Log("<color=magenta>'LogRewardedVideoAdStart' call</color>");
        AnalyticsController.LogRewardedVideoAdStart(levelnumber);
    }

    public static void LogRewardedVideoAdComplete(int levelnumber)
    {
        Debug.Log("<color=magenta>'LogRewardedVideoAdComplete' call</color>");
        AnalyticsController.LogRewardedVideoAdComplete(levelnumber);
    }

    public static void LogAdWatchOneTimeAppsFlyerIfApplicable(int lvNow)
    {
        List<int> lvList = GameConfig.adWatchNTimesCompletedAppsflyerHD.AllNumbers;
        if (lvNow >= 1)
        {
            foreach (var v in lvList)
            {
                HardData<bool> hasAdWatched = GameConfig.adWatchNTimesCompletedAppsflyerHD.GetConfigForLevel(v);// new HardData<bool>("ads_watched_HD_" + v, false);
                if (hasAdWatched.value == false && lvNow >= v)
                {
                    string adWatchEventToken = "";
                    try
                    {
                        adWatchEventToken = GameConfig.adWatchAdjustEvents[v];
                    }
                    catch (System.Exception ex)
                    {
                        Debug.Log("token retrieve error, please check the dictionary in 'GameConfig' script.");
                    }
                    AdjustEvent adjustEvent = new AdjustEvent(adWatchEventToken);
                    Adjust.trackEvent(adjustEvent);
                    Debug.Log("<color='magenta'>ad watch num: " + v + "</color>");
                    hasAdWatched.value = true;
                }
            }
        }
    }


    public static void LogPurchase(string currencyCode, string amount, string transactionID)
    {
        
        string purchaseEventToken = "mzap7z";
#if UNITY_ANDROID
        purchaseEventToken = "mzap7z";
#elif UNITY_IOS
        purchaseEventToken = "9m1js7";
#endif

        AdjustEvent adjustEvent = new AdjustEvent(purchaseEventToken);
        double revAmount = 0;
        double.TryParse(amount, out revAmount);
        adjustEvent.setRevenue(revAmount, currencyCode);
        adjustEvent.setTransactionId(transactionID);
        Adjust.trackEvent(adjustEvent);

        Debug.Log("<color='magenta'>a_f purchase analytics method</color>");

    }
}