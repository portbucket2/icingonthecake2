﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEditor;
using FRIA;
public class GalleryManager : MonoBehaviour
{
    public static HardData<int> _galleryItemCountHD;
    public static int GalleryItemCount
    {
        get
        {
            if (_galleryItemCountHD == null) _galleryItemCountHD = new HardData<int>("GALLERY_ITEM_COUNT", 0);
            return _galleryItemCountHD.value;
        }
        set
        {
            if (_galleryItemCountHD == null) _galleryItemCountHD = new HardData<int>("GALLERY_ITEM_COUNT", 0);
            _galleryItemCountHD.value = value;
        }
    }
    // Start is called before the first frame update
    public GameObject galleryRootObject;
    public GameObject galleryItemUIPrefab;
    public RectTransform galleryContentRoot;

    public List<GalleryItemData> gidList = new List<GalleryItemData>();
    public Dictionary<string,GalleryItemVcolorInfo> vcolDataKeeper = new Dictionary<string, GalleryItemVcolorInfo>();
    public List<GalleryUIItem> UIItemList;

    public static GalleryManager instance;
    private void Awake()
    {
        instance = this;
        string subRoot = "";
        if (string.IsNullOrEmpty(subRoot))
        {
#if UNITY_EDITOR
            subRoot = Path.Combine(Application.dataPath, "SerializedStorage/GalleryData");
#else
            subRoot = Path.Combine(Application.persistentDataPath,"SerializedStorage/GalleryData");
#endif
        }


        if (!Directory.Exists(subRoot))
        {
            Directory.CreateDirectory(subRoot);
        }

        galleryRootObject.SetActive(false);
        GXMLSerializer.InitializeRootForThreading();
        //StartCoroutine(FileWriterThreadRoutine());

        readVcolThread = new ThreadHandler_GalleryVColRead(this);
        writeVcolThread = new ThreadHandler_GalleryVColWrite(this);

    }
    ThreadHandler_GalleryVColWrite writeVcolThread;
    ThreadHandler_GalleryVColRead readVcolThread;



    
    
    private void Start()
    {
        UpdateGIDList();
    }


    public GameObject NoItemIndication;
    public GameObject ScrollPanel;
    //editor reference
    public void OnGalleryLoad()
    {
        Refresh();
        NoItemIndication.SetActive(GalleryItemCount == 0);
        ScrollPanel.SetActive(GalleryItemCount > 0);
    }



    private void UpdateGIDList()
    {
        gidList.Clear();
        for (int i = 0; i < GalleryItemCount; i++)
        {
            gidList.Add(LoadGalleryItemData(i));
        }

        Refresh();
    }
    public void Refresh()
    {
        //Debug.LogFormat("Refreshing Count {0}",GalleryItemCount);
        for (int i = UIItemList.Count-1; i >=0; i--)
        {
            Pool.Destroy(UIItemList[i].gameObject);
            UIItemList.RemoveAt(i);
        }
        for (int i = 0; i < GalleryItemCount; i++)
        {
            GalleryUIItem UIItem = Pool.Instantiate(galleryItemUIPrefab, galleryContentRoot).GetComponent<GalleryUIItem>();
            UIItem.transform.localScale = Vector3.one;
            UIItemList.Add(UIItem);
            int index = i;
            GalleryItemData gid = gidList[index];
            UIItem.Load(gid,()=> {
                try
                {
                    if (gid.vcolData == null)
                    {
                        if (!vcolDataKeeper.ContainsKey(gid.vcolDataPath))
                        {
                            Debug.LogFormat("<color='orange'>Direct Load {0}</color>", index);
                            vcolDataKeeper.Add(gid.vcolDataPath, GXMLSerializer.Load<GalleryItemVcolorInfo>(gid.vcolDataPath));
                        }

                        gid.vcolData = vcolDataKeeper[gid.vcolDataPath];
                    }

                }
                catch (System.Exception e)
                {
                    Debug.LogError(e.Message);
                    return;
                }
                
                galleryRootObject.SetActive(false);
                recentGalleryItemData = gid;
            });
        }
    }
    public static GalleryItemData recentGalleryItemData;

    public GalleryItemData LoadGalleryItemData(int id)
    {
        string path = string.Format("GalleryData/{0}_info", id);
        GalleryItemData gid = GXMLSerializer.Load<GalleryItemData>(path);
        //gid.vcolData = GXMLSerializer.Load<GalleryItemVcolorInfo>(gid.vcolDataPath);
        return gid;
    }
    public GalleryItemData SaveGalleryItemData()
    {
        if (writeVcolThread.isBusy)
        {
            Debug.LogError("gallery data not saved due to thread being busy");

            return null;
        }
        string path = string.Format("GalleryData/{0}_info", GalleryItemCount);
        GalleryItemData gid = new GalleryItemData();
        gid.levelDefinition = LevelPrefabManager.currentLevel.levelDefinition;
        gid.ai = LevelPrefabManager.currentLevel.areaI;
        gid.li = LevelPrefabManager.currentLevel.levelI;
        gid.date = string.Format("{0:MMM} {0:dd},{0:yy}", System.DateTime.Now);
        gid.time = string.Format("{0:hh}:{0:mm} {0:tt}", System.DateTime.Now);
        gid.vcolDataPath =  string.Format("GalleryData/{0}_vcols", GalleryItemCount);
        gid.pngDataPath = string.Format("GalleryData/{0}_texture", GalleryItemCount);
        
        //gidTargetForVcolSave = gid;
        GXMLSerializer.Save<GalleryItemData>(gid, path);

        PhaseController_Icing icephase = PhaseManager.instance.GetPhase(GamePhase.AddCream) as PhaseController_Icing;
        GalleryItemVcolorInfo vcolDataToSave = new GalleryItemVcolorInfo();
        vcolDataToSave.colors = icephase.GetColorForGalleryData();
        vcolDataToSave.baseColor = icephase.morfMeshes[0].GetComponent<MeshRenderer>().sharedMaterial.GetColor("_Color0");
        //vcolDatapath = gid.vcolDataPath;

        writeVcolThread.ScheduleNewSave(gid, vcolDataToSave);

        return gid;
    }

    //public GalleryItemData dataAwaitingPhoto;
    public void SaveGalleryImage(Camera decoCam, GalleryItemData gid)
    {
        decoCam.enabled = true;
        //Debug.LogFormat("<color='yellow'>saving snap</color>");
        RenderTexture rendTex = new RenderTexture(256, 256, 24);
        decoCam.targetTexture = rendTex;
        decoCam.Render();
        Texture2D texture = new Texture2D(rendTex.width, rendTex.height, TextureFormat.RGBA32, false);

        RenderTexture.active = rendTex;

        texture.ReadPixels(new Rect(0, 0, rendTex.width, rendTex.height), 0, 0, false);
        //texture.alphaIsTransparency = true;
        texture.Apply();
        RenderTexture.active = null;

        byte[] bytes = texture.EncodeToPNG();
        decoCam.targetTexture = null;
        DestroyImmediate(rendTex);
        System.IO.File.WriteAllBytes(gid.GetFullImagePath(), bytes);
        decoCam.enabled = false;
    }



}

[System.Serializable]
public class GalleryItemData
{
    //[XmlArray("IGAlert"), XmlArrayItem("IGAlertItem")]
    public LevelDefinition levelDefinition;
    public int ai;
    public int li;
    public string date;
    public string time;
    public string pngDataPath;
    public string vcolDataPath;
    [System.NonSerialized][XmlIgnore]
    public GalleryItemVcolorInfo vcolData;

    public string GetFullImagePath()
    {
#if UNITY_EDITOR
        return string.Format("{0}/SerializedStorage/{1}.png", Application.dataPath, pngDataPath);
#else
        
        return string.Format("{0}/SerializedStorage/{1}.png",Application.persistentDataPath, pngDataPath);        
#endif
    }
}


[System.Serializable]
public class GalleryItemVcolorInfo
{
    public Vector4 baseColor;
    public Vector4[] colors;
}

//[System.Serializable]
//public class GalleryItemVcolorInfo
//{
//    public Vector4[] colors;
//}


//#if UNITY_EDITOR
//[CustomEditor(typeof(GalleryManager))]
//public class GalleryManagerEditor : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();
//        GalleryManager galleryMan = (GalleryManager)target;
//        if (LevelPrefabManager.currentLevel)
//        {
//            if (GUILayout.Button("SaveData", GUILayout.Height(50)))
//            {
//                galleryMan.SaveGalleryItemData();
//            }
//        }
//    }
//}
//#endif