﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRIA
{
    public class CSVReader : MonoBehaviour
    {

        public string path;
        public char commaCharacter = ',';
        void Start()
        {
            foreach (var csv in ReadFromResource(path, commaCharacter))
            {
                string s = "";
                foreach (string item in csv.fields)
                {
                    s += "#" + item + " ";
                }
                Debug.Log(s);
            }
        }


        public static CSVRow[] ReadFromResource(string path, char commaCharacter)
        {
            TextAsset csvAsset = Resources.Load<TextAsset>(path);
            return ReadCSVAsset(csvAsset, commaCharacter);
        }
        public static CSVRow[] ReadCSVAsset(TextAsset csvAsset, char commaCharacter)
        {
            string[] rows = csvAsset.text.Split(new char[] { '\n' });

            CSVRow[] csvRows = new CSVRow[rows.Length - 1];

            for (int i = 0; i < csvRows.Length; i++)
            {
                csvRows[i] = new CSVRow(rows[i + 1].Split(new char[] { ',' }), commaCharacter);
            }

            return csvRows;
        }


        public static Dictionary<string, CSVRow> ReadFromResource(string path, int keyIndex, char commaCharacter)
        {
            TextAsset csvAsset = Resources.Load<TextAsset>(path);
            return ReadCSVAsset(csvAsset, keyIndex, commaCharacter);
        }
        public static Dictionary<string, CSVRow> ReadCSVAsset(TextAsset csvAsset, int keyIndex, char commaCharacter)
        {
            string[] rows = csvAsset.text.Split(new char[] { '\n' });

            Dictionary<string, CSVRow> dic = new Dictionary<string, CSVRow>(rows.Length - 1); //first row ignored

            for (int i = 0; i < rows.Length - 1; i++)
            {
                CSVRow csvRow = new CSVRow(rows[i + 1].Split(new char[] { ',' }), commaCharacter);

                dic.Add(csvRow.fields[keyIndex], csvRow);

            }

            return dic;
        }


    }
    public class CSVRow
    {
        public string[] fields;
        public CSVRow(string[] fields, char commaSubstitute = ',')
        {
            if (commaSubstitute != ',')
            {
                this.fields = new string[fields.Length];
                for (int i = 0; i < fields.Length; i++)
                {
                    this.fields[i] = fields[i].Replace(commaSubstitute, ',');
                }
            }
            else
            {
                this.fields = fields;
            }
        }
    }
}