﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InLevelCanvasManager : MonoBehaviour
{
    public SuccessBoxController successController;
    public GameObject failureObj;
    public GameObject minimizeObj;

    public List<GameObject> stars;



    [Header("Fail")]
    public Button restartButton;
    public Button levelsButton;

    //[Header("Succ")]
    //public Button levels2Button;
    //public Button replayButton;
    //public Button nextButton;

    [Header ( "rating" )]
    public Button giveRating;
    public Button skipRating;
    public GameObject ratingwindow;

    [Header("Ext")]
    public Button restartButtonExt;
    public Button levelsButtonExt;
    [Header("Minimize")]
    public Button miniModMinimizeButton;
    //public Button miniModNextButton;
    public GameObject miniModInitialPart;
    public GameObject miniModLaterPart;


    [Header("misc")]

    private LevelPrefabManager levelObject;
    public Text levelNameText;
    //public Text infoText;

    private void Awake()
    {

        PhaseController_Decorate.decorationComplete += OnDecorationComplete;
    }
    private void OnDestroy()
    {

        PhaseController_Decorate.decorationComplete += OnDecorationComplete;
    }
    public  void LevelStart(LevelPrefabManager levelObj)
    {
        levelObject = levelObj;
        failureObj.SetActive(false);
        miniModInitialPart.SetActive(false);
        miniModLaterPart.SetActive(false);
        successController.HideAll();

        levelNameText.text = LevelLoader.GetLevelName(levelObj.areaI,levelObj.levelI);

        miniModMinimizeButton.onClick.RemoveAllListeners();
        miniModMinimizeButton.onClick.AddListener(OnMini);

        restartButton.onClick.RemoveAllListeners();
        restartButton.onClick.AddListener(levelObject.OnReset);


        levelsButton.onClick.RemoveAllListeners();
        levelsButton.onClick.AddListener(levelObject.OnLevels);


        restartButtonExt.onClick.RemoveAllListeners();
        restartButtonExt.onClick.AddListener(levelObject.OnReset);

        levelsButtonExt.onClick.RemoveAllListeners();
        levelsButtonExt.onClick.AddListener(levelObject.OnLevels);
        SetStars(0);

        levelsButtonExt.gameObject.SetActive(LevelLoader.instance.testMode);


    }



    public void OnDecorationComplete()
    {
        successController.decoratePanel.SetActive(true);
    }
    //public Animator nextButtonAnim;



    public void SetStars(int starCount)
    {
        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].SetActive(starCount > i);
        }
    }


    private void OnMini()
    {

        successController.HideAll();
        miniModInitialPart.SetActive(false);
        miniModLaterPart.SetActive(true);
        onMiniAction?.Invoke();
    }
    public void LoadFail()
    {
        failureObj.SetActive(true);
        //restartButtonExt.gameObject.SetActive(false);
    }
    System.Action onMiniAction;
    public void LoadSucces(string infoString, System.Action onMiniAction)
    {
        this.onMiniAction = onMiniAction;
        //infoText.text = infoString;
        //successObj.SetActive(true);
        //miniModInitialPart.SetActive(true);
        miniModLaterPart.SetActive(false);

        successController.Show_Success();
    }


    public void LoadRatingWindow ()
    {
        ratingwindow.SetActive ( true );

        skipRating.onClick.AddListener ( () =>
        {
            ratingwindow.SetActive ( false );
            //successObj.SetActive ( true );
            //miniModInitialPart.SetActive ( true );
            //miniModLaterPart.SetActive ( false );
            //onSuccessLoad?.Invoke ();
        } );
        giveRating.onClick.AddListener ( () =>
        {
            Application.OpenURL ( "market://details?id=" + Application.identifier + "&reviewId=0" );

            //Application.OpenURL ( "itms-apps://itunes.apple.com/app/idYOUR_ID" );
            ratingwindow.SetActive ( false );
            //successObj.SetActive ( true );
            //miniModInitialPart.SetActive ( true );
            //miniModLaterPart.SetActive ( false );
            //onSuccessLoad?.Invoke ();
        } );

    }
}
